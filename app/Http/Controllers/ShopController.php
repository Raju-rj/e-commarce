<?php

namespace App\Http\Controllers;

use App\Category;
use App\Page;
use App\Product;
use App\Review;
use App\Slider;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index()
    {
        $newCategories = Category::where('publication_status', 1)->get();
        $newProducts = Product::where('publication_status', 1)
            ->orderBy('id', 'DESC')
            ->take(8)
            ->get();

        $newPages = Page::where('publication_status', 1)
            ->orderBy('id', 'ASC')
            ->take(7)
            ->get();

        $newSliders = Slider::where('publication_status', 1)
            ->orderBy('id', 'ASC')
            ->take(7)
            ->get();

        return view('front-end/home/index', [
            'newCategories' => $newCategories,
            'newSliders' => $newSliders,
            'newProducts' => $newProducts,
            'newPages' => $newPages

        ]);
    }

    public function categoryProduct($id)
    {
        $category = Category::find($id);
        $categoryProducts = Product::where('category_id', $id)
            ->where('publication_status', 1)
            ->get();

        return view('front-end.categories.category-content', [
            'category' => $category,
            'categoryProducts' => $categoryProducts
        ]);
    }


    public function productDetails($id)
    {
        $reviews = Review::all();
        $product = Product::find($id);
        return view('front-end.products.product-details', [
            'product' => $product,
            'reviews' => $reviews
        ]);
    }

    public function coustomerReview(Request $request, $id)
    {
        $reviews = Review::all();

        $product = Product::find($id);
        $customerReview = new Review();
        $customerReview->name = $request->name;
        $customerReview->email_address =$request->email_address;
        $customerReview->review_title = $request->review_title;
        $customerReview->comments = $request->comments;
        $customerReview->save();

        return view('front-end.products.product-details', [
            'product' => $product,
            'reviews' => $reviews

        ]);

    }


}
