<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Order;
use App\OrderDetails;
use App\Shipping;
use Illuminate\Http\Request;
use Session;
use Cart;
use Mail;

class CheckoutController extends Controller
{
//    public function customerIndex()
//    {
//        return view('front-end.customer.register');
//    }


    public function index()
    {
        $customer = Customer::find(Session::get('customerId'));

        $cartProducts = Cart::content();

        return view('front-end.checkout.checkout-content', [
            'customer'=>$customer,
            'cartProducts'=>$cartProducts
        ]);
    }

    public function saveShippingInfo(Request $request)
    {
        $shipping = new Shipping();
        $shipping->name = $request->name;
        $shipping->email_address = $request->email_address;
        $shipping->phone_number = $request->phone_number;
        $shipping->address = $request->address;
        $shipping->payment_type = $request->payment_type;
        $shipping->save();

        $shippingId = $shipping->id;
        Session::put('shippingId', $shippingId);
//        Session::put('name', $shipping->name);

        $data = $shipping->toArray();
        Mail::send('front-end.mails.confirmation-mail', $data, function ($message) use ($data){
            $message->to($data['email_address']);
            $message->subject('Confirmation mail');
        });

        $order = new Order();
        $order->shipping_id = Session::get('shippingId');
        $order->order_total = Session::get('orderTotal');
        $order->save();

        $cartProducts = Cart::content();
        foreach ($cartProducts as $cartProduct)
        {
            $orderDetail = new OrderDetails();
            $orderDetail->order_id = $order->id;
            $orderDetail->product_id = $order->id;
            $orderDetail->product_name = $cartProduct->name;
            $orderDetail->product_price = $cartProduct->price;
            $orderDetail->product_quantity = $cartProduct->qty;
            $orderDetail->save();
        }

        Cart::destroy();

        return redirect('/complete/order');
    }

    public function completeOrder()
    {
        return 'success';
    }




}
