<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Image;
use DB;
class ProductController extends Controller
{
    public function index()
    {

        $categories = Category::where('publication_status', 1)->get();
        $brands = Brand::where('publication_status', 1)->get();

        return view('admin/product/add-product', [
            'categories' => $categories,
            'brands' => $brands,
        ]);
    }

    public function saveProduct(Request $request)
    {
        $productImage = $request->file('product_image');
        $fileType = $productImage->getClientOriginalExtension();
        $imageName = $request->product_name . '.' . $fileType;
        $directory = 'product-images/';
        $imageUrl = $directory . $imageName;
//       $productImage->move($directory, $imageName);
        Image::make($productImage)->save($imageUrl);

        $product = new Product();
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->product_name = $request->product_name;
        $product->product_price = $request->product_price;
        $product->product_quantity = $request->product_quantity;
        $product->short_description = $request->short_description;
        $product->long_description = $request->long_description;
        $product->publication_status = $request->publication_status;
        $product->product_image = $imageUrl;
        $product->save();

        return redirect('/product/add-product')->with('message', 'Product Info Save successfully!!');

    }

    public function manageProduct()
    {
        $products = DB::table('products')
                        ->join('categories', 'products.category_id', '=', 'categories.id')
                        ->join('brands', 'products.brand_id', '=', 'brands.id')
                        ->select('products.*', 'categories.category_name', 'brands.brand_name')
                        ->get();

        return view('admin/product/manage-product', ['products'=>$products]);

    }

    public function unpublished($id){
        $product = Product::find($id);
        $product->publication_status = 0;
        $product->save();
        return redirect('/product/manage-product')->with('unpublished', 'Product Unpublished!!');

    }

    public function published($id){
        $product = Product::find($id);
        $product->publication_status = 1;
        $product->save();
        return redirect('/product/manage-product')->with('published', 'Product Published!!');
    }


    public function editProduct($id)
    {
        $product = Product::find($id);
        $categories = Category::where('publication_status', 1)->get();
        $brands = Brand::where('publication_status', 1)->get();

        return view('admin/product/edit-product', [
            'product'=>$product,
            'categories' => $categories,
            'brands' => $brands
         ]);
    }

    public function updateProduct(Request $request)
    {
        $product = Product::find($request->product_id);

        $productImage = $request->file('product_image');
       if ($productImage){
           unlink($product->product_image);
           $fileType = $productImage->getClientOriginalExtension();
           $imageName = $request->product_name . '.' . $fileType;
           $directory = 'product-images/';
           $imageUrl = $directory . $imageName;
           Image::make($productImage)->save($imageUrl);

           $product->category_id = $request->category_id;
           $product->brand_id = $request->brand_id;
           $product->product_name = $request->product_name;
           $product->product_price = $request->product_price;
           $product->product_quantity = $request->product_quantity;
           $product->short_description = $request->short_description;
           $product->long_description = $request->long_description;
           $product->product_image = $imageUrl;
           $product->publication_status = $request->publication_status;
           $product->save();

       } else {

           $product->category_id = $request->category_id;
           $product->brand_id = $request->brand_id;
           $product->product_name = $request->product_name;
           $product->product_price = $request->product_price;
           $product->product_quantity = $request->product_quantity;
           $product->short_description = $request->short_description;
           $product->long_description = $request->long_description;
           $product->publication_status = $request->publication_status;
           $product->save();
       }
        return redirect('/product/manage-product')->with('update', 'Product Info Updated!!');
    }

    public function deleteProduct($id){
        $product = Product::find($id);
        $product->delete();
        return redirect('product/manage-product')->with('delete', 'Product info Deleted!!');
    }
}
