<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use DB;


class CategoryController extends Controller
{
    public function add()
    {
        return view('admin/category/add-category');
    }

    public function saveCategory(Request $request)
    {

        $this->validate($request, [
            'category_name' => 'required|regex:/^[\pL\s\-]+$/u|max:20|min:3',
            'category_description' => 'required',
            'publication_status' => 'required'
        ]);


        $category = new Category();
        $category->category_name = $request->category_name;
        $category->category_description = $request->category_description;
        $category->publication_status = $request->publication_status;
        $category->save();

        return redirect('category/add-category')->with('message', 'Category add successfully');
    }

    public function manageCategory()
    {
        $categories = Category::all();
        return view('admin/category/manage-category', ['categories'=>$categories]);
    }

    public function unpublished($id){
        $category = Category::find($id);
        $category->publication_status = 0;
        $category->save();
        return redirect('/category/manage')->with('unpublished', 'Category Unpublished!!');

    }

    public function published($id){
        $category = Category::find($id);
        $category->publication_status = 1;
        $category->save();
        return redirect('/category/manage')->with('published', 'Category Published!!');
    }

    public function editCategory($id){
        $category = Category::find($id);
        return view('admin/category/edit-category', ['category'=>$category]);
    }

    public function updateCategory(Request $request){
        $category = Category::find($request->category_id);

        $category->category_name = $request->category_name;
        $category->category_description = $request->category_description;
        $category->publication_status = $request->publication_status;
        $category->save();

        return redirect('category/manage')->with('update', 'Category info update suceesfully!');
    }

    public function deleteCategory($id){
        $category = Category::find($id);
        $category->delete();
        return redirect('category/manage')->with('delete', 'Category info delete suceesfully!');
    }
}
