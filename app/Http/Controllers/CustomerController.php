<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Review;
use App\User;
use Illuminate\Http\Request;
use Session;
use Auth;


class CustomerController extends Controller
{

//    public function customerLogout()
//    {
//        Session::forget('customerId');
//        Session::forget('name');
//
//        return redirect('/');
//    }

    public function coustomerIndex()
    {
        return view('front-end.customer.register');
    }

    public function coustomerSingUp(Request $request)
    {
        $this->validation($request);
        $request['password'] = bcrypt($request->password);
        User::create($request->all());
        return redirect('/customer/login')->with('registed', 'Registred successfully');



//        $customer = new Customer();
//        $customer->first_name = $request->first_name;
//        $customer->last_name = $request->last_name;
//        $customer->email_address =$request->email_address;
//        $customer->password = bcrypt($request->password);
//        $customer->address = $request->address;
//        $customer->phone_number = $request->phone_number;
//        $customer->save();
//
//        $customerId = $customer->id;
//        Session::put('customerId', $customerId);
//        Session::put('name', $customer->first_name.' '.$customer->last_name);



//        return redirect('/customer/login');
    }

    public function validation($request)
    {
        return $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone_number' => 'required',
            'address' => 'required',
        ]);
    }



    public function loginIndex()
    {
        return view('front-end.customer.login');
    }

    public function customerLoginCheck(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password])){
            return redirect('/');
        }
        else{
            return redirect('/customer/login')->with('error', 'Email or Password does not match.');
        }

//        $customer = Customer::where('email_address', $request->email_address)->first();
//        if (password_verify($request->password, $customer->password)) {
//            Session::put('customerId', $customer->id);
//            Session::put('name', $customer->first_name . ' ' . $customer->last_name);
//            return redirect('/');
//        } else {
//            return redirect('/customer/login')->with('message', 'Invalid Password!');
//        }
    }



}
