<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function add()
    {
        return view('admin/brand/add-brand');
    }

    public function saveBrand(Request $request)
    {

        $this->validate($request, [
            'brand_name' => 'required|regex:/^[\pL\s\-]+$/u|max:20|min:3',
            'brand_description' => 'required',
            'publication_status'
        ]);


        $brand = new Brand();
        $brand->brand_name = $request->brand_name;
        $brand->brand_description = $request->brand_description;
        $brand->publication_status = $request->publication_status;
        $brand->save();

        return redirect('brand/add-brand')->with('message', 'Brand add successfully');
    }

    public function manageBrand()
    {
        $brands = Brand::all();
        return view('admin/brand/manage-brand', ['brands'=>$brands]);
    }

    public function unpublished($id){
        $brand = Brand::find($id);
        $brand->publication_status = 0;
        $brand->save();
        return redirect('/brand/manage')->with('unpublished', 'Brand Unpublished!!');

    }

    public function published($id){
        $brand = Brand::find($id);
        $brand->publication_status = 1;
        $brand->save();
        return redirect('/brand/manage')->with('published', 'Brand Published!!');
    }

    public function editBrand($id){
        $brand = Brand::find($id);
        return view('admin/brand/edit-brand', ['brand'=>$brand]);
    }

    public function updateBrand(Request $request){
        $brand = Brand::find($request->brand_id);

        $brand->brand_name = $request->brand_name;
        $brand->brand_description = $request->brand_description;
        $brand->publication_status = $request->publication_status;
        $brand->save();

        return redirect('brand/manage')->with('update', 'Brand info update suceesfully!');
    }

    public function deleteBrand($id){
        $category = Brand::find($id);
        $category->delete();
        return redirect('brand/manage')->with('delete', 'Brand info delete suceesfully!');
    }
}
