<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Order;
use App\OrderDetails;
use App\Shipping;
use Illuminate\Http\Request;
use DB;
use PDF;

class OrderController extends Controller
{
    public function mangeOrderInfo()
    {
              $orders = DB::table('orders')
                        ->join('shippings', 'orders.shipping_id', '=', 'shippings.id')
                        ->select('orders.*', 'shippings.name',  'shippings.payment_type', 'shippings.payment_status')
                        ->get();

        return view('admin.order.manage-order', ['orders'=> $orders]);
    }

    public function viewOrderDetail($id)
    {
        $order = Order::find($id);
        $shipping = Shipping::find($order->shipping_id);
        $orderDetails = OrderDetails::where('order_id', $order->id)->get();

        return view('admin.order.view-order',[
            'order' => $order,
            'shipping' => $shipping,
            'orderDetails' => $orderDetails
        ]);
    }

    public function viewOrderInvoice($id)
    {
        $order = Order::find($id);
        $customer = Customer::find($id);
        $shipping = Shipping::find($order->shipping_id);
        $orderDetails = OrderDetails::where('order_id', $order->id)->get();

        return view('admin.order.view-order-invoice', [
            'order' => $order,
            'customer' => $customer,
            'shipping' => $shipping,
            'orderDetails' => $orderDetails
        ]);
    }

    public function downloadOrderInvoice($id)
    {
        $order = Order::find($id);
        $customer = Customer::find($id);
        $shipping = Shipping::find($order->shipping_id);
        $orderDetails = OrderDetails::where('order_id', $order->id)->get();

        $pdf = PDF::loadView('admin.order.download-invoice', [
            'order' => $order,
            'customer' => $customer,
            'shipping' => $shipping,
            'orderDetails' => $orderDetails
        ]);
        return $pdf->stream('invoice.pdf');
    }

}
