<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;


class SliderController extends Controller
{
    public function index()
    {

        return view('admin.slider.add-slider');
    }

    public function saveSlider(Request $request)
    {
        $sliderImage = $request->file('slider_image');
        $fileType = $sliderImage->getClientOriginalExtension();
        $imageName = $request->slider_title . '.' . $fileType;
        $directory = 'slider-images/';
        $imageUrl = $directory . $imageName;
        Image::make($sliderImage)->save($imageUrl);

        $slider = new Slider();
        $slider->slider_title = $request->slider_title;
        $slider->publication_status = $request->publication_status;
        $slider->slider_image = $imageUrl;
        $slider->save();

        return redirect('/slider/add-slider')->with('massage', 'Slider info save successfully');
    }

    public function manageSlider()
    {
        $sliders = Slider::all();
        return view('admin/slider/manage-slider', ['sliders'=>$sliders]);
    }

    public function unpublished($id){
        $slider = Slider::find($id);
        $slider->publication_status = 0;
        $slider->save();
        return redirect('/slider/manage')->with('unpublished', 'Slider Unpublished!!');

    }

    public function published($id){
        $slider = Slider::find($id);
        $slider->publication_status = 1;
        $slider->save();
        return redirect('/slider/manage')->with('published', 'Slider Published!!');
    }

    public function editSlider($id){
        $slider = Slider::find($id);
        return view('admin/slider/edit-slider', ['slider'=>$slider]);
    }

    public function updateSlider(Request $request){
        $slider = Slider::find($request->category_id);

        $slider->slider_title = $request->slider_title;
        $slider->slider_iamge = $request->slider_iamge;
        $slider->publication_status = $request->publication_status;
        $slider->save();

        return redirect('slider/manage')->with('update', 'Slider info update suceesfully!');
    }

    public function deleteSlider($id){
        $slider = Slider::find($id);
        $slider->delete();
        return redirect('slider/manage')->with('delete', 'Slider info delete suceesfully!');
    }
}
