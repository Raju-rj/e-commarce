<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    public function index()
    {
       return view('admin/subscriber/add-subscriber');
    }

    public function saveEmail(Request $request)
    {
        $email = new Subscriber();
        $email->email = $request->email;
        $email->save();
        return redirect('subscriber/add-subscriber')->with('message', 'Email Save Successfully!!');
    }

    public function manageEmail()
    {
        $emails = Subscriber::all();
        return view('admin/subscriber/manage-subscriber', ['emails' => $emails]);
    }

    public function deleteSubscriber($id)
    {
        $email = Subscriber::find($id);
        $email->delete();
        return redirect('subscriber/manage-subscriber')->with('delete', 'Email info delete suceesfully!');
    }
}
