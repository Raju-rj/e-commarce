<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        return view('admin/page/add-page');
    }

    public function savePage(Request $request)
    {
        $page = new Page();
        $page->page_title = $request->page_title;
        $page->page_content = $request->page_content;
        $page->publication_status = $request->publication_status;
        $page->save();
        return redirect('page/add-page')->with('message', 'Page Save Successfully!!');
    }

    public function managePage()
    {
        $pages = Page::all();
        return view('admin/page/manage-page', ['pages' =>$pages]);
    }


    public function unpublished($id)
    {
        $page = Page::find($id);
        $page->publication_status = 0;
        $page->save();
        return redirect('page/manage-page')->with('unpublished', 'Page Unpublished');
    }
    public function published($id)
    {
        $page = Page::find($id);
        $page->publication_status = 1;
        $page->save();
        return redirect('page/manage-page')->with('published', 'Page Published');
    }


    public function editPage($id){
        $page = Page::find($id);
        return view('admin/page/edit-page', ['page'=>$page]);
    }

    public function updatePage(Request $request){
        $page = Page::find($request->page_id);

        $page->page_title = $request->page_title;
        $page->page_content = $request->page_content;
        $page->publication_status = $request->publication_status;
        $page->save();

        return redirect('page/manage-page')->with('update', 'Page info update suceesfully!');
    }

    public function deletePage($id)
    {
        $page = Page::find($id);
        $page->delete();
        return redirect('page/manage-page')->with('delete', 'Page Delete Successfully!!');
    }
 public function showPage($id)
    {
        $page = Page::find($id);

        return view('front-end/about/about')->with('page', $page);
    }


}
