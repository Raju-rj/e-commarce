<?php

namespace App\Providers;

use App\Category;
use App\Product;
use Illuminate\Support\ServiceProvider;
use View;
use Cart;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer('front-end.includes.header', function ($view){
            $view->with('newCategories', Category::where('publication_status', 1)->get());

        });

        View::composer('front-end.includes.header', function ($view){
           $view->with('cartProducts', Cart::content(), Product::all());

        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
