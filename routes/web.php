<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'ShopController@index',
    'as'   => '/'
]);

Route::get('/about', [
    'uses' => 'ShopController@about',
    'as'   => '/'
]);


Route::get('/admin', function () {
    return view('admin-layouts/master-layout');
});

Route::get('/slider/add-slider', 'SliderController@index');

Route::post('/slider/save', [
    'uses' => 'SliderController@saveSlider',
    'as' => 'new-slider'
]);

Route::get('/slider/manage', [
    'uses' => 'SliderController@manageSlider',
    'as' => 'manage'
]);

Route::get('/slider/unpublished/{id}', [
    'uses' => 'SliderController@Unpublished',
    'as' => 'slider-unpublished'
]);

Route::get('/slider/published/{id}', [
    'uses' => 'SliderController@published',
    'as' => 'slider-published'
]);

Route::get('/slider/edit/{id}', [
    'uses' => 'SliderController@editSlider',
    'as' => 'edit-slider'
]);

Route::post('/slider/update', [
    'uses' => 'SliderController@updateSlider',
    'as' => 'update-slider'
]);

Route::get('/slider/delete/{id}', [
    'uses' => 'SliderController@deleteSlider',
    'as' => 'delete-slider'
]);

Route::get('/category/add-category', 'CategoryController@add');

Route::post('/category/save', [
    'uses' => 'CategoryController@saveCategory',
    'as' => 'new-category'
]);


Route::get('/category/manage', [
    'uses' => 'CategoryController@manageCategory',
    'as' => 'manage'
]);

Route::get('/category/unpublished/{id}', [
    'uses' => 'CategoryController@unpublished',
    'as' => 'category-unpublished'
]);

Route::get('/category/published/{id}', [
    'uses' => 'CategoryController@published',
    'as' => 'category-published'
]);

Route::get('/category/edit/{id}', [
    'uses' => 'CategoryController@editCategory',
    'as' => 'edit-category'
]);

Route::post('/category/update', [
    'uses' => 'CategoryController@updateCategory',
    'as' => 'update-category'
]);

Route::get('/category/delete/{id}', [
    'uses' => 'CategoryController@deleteCategory',
    'as' => 'delete-category'
]);


Route::get('/brand/add-brand', 'BrandController@add');

Route::post('/brand/save', [
    'uses' => 'BrandController@saveBrand',
    'as' => 'new-brand'
]);

Route::get('/brand/manage', [
    'uses' => 'BrandController@manageBrand',
    'as' => 'manage'
]);

Route::get('/brand/unpublished/{id}', [
    'uses' => 'BrandController@unpublished',
    'as' => 'unpublished'
]);

Route::get('/brand/published/{id}', [
    'uses' => 'BrandController@published',
    'as' => 'published'
]);

Route::get('/brand/edit/{id}', [
    'uses' => 'BrandController@editBrand',
    'as' => 'edit-brand'
]);

Route::post('/brand/update', [
    'uses' => 'BrandController@updateBrand',
    'as' => 'update-brand'
]);

Route::get('/brand/delete/{id}', [
    'uses' => 'BrandController@deleteBrand',
    'as' => 'delete-brand'
]);

Route::get('/product/add-product', 'ProductController@index');

Route::post('/product/save', [
    'uses' => 'ProductController@saveProduct',
    'as' => 'new-product'
]);

Route::get('/product/manage-product', [
    'uses' => 'ProductController@manageProduct',
    'as' => 'manage-product'
]);

Route::get('/product/edit/{id}', [
    'uses' => 'ProductController@editProduct',
    'as' => 'edit-product'
]);

Route::get('/product/Unpublished/{id}', [
    'uses' => 'ProductController@unpublished',
    'as' => 'product-unpublished'
]);

Route::get('/product/published/{id}', [
    'uses' => 'ProductController@published',
    'as' => 'product-published'
]);

Route::post('/product/update', [
    'uses' => 'ProductController@updateProduct',
    'as' => 'update-product'
]);

Route::get('/product/delete/{id}', [
    'uses' => 'ProductController@deleteProduct',
    'as' => 'delete-product'
]);



Route::get('/subscriber/add-subscriber', 'SubscriberController@index');

Route::post('/subscriber/save', [
    'uses' => 'SubscriberController@saveEmail',
    'as' => 'new-email'
]);

Route::get('/subscriber/manage-subscriber', [
    'uses' => 'SubscriberController@manageEmail',
    'as' => 'manage-subscriber'
]);

Route::get('/subscriber/delete/{id}', [
    'uses' => 'SubscriberController@deleteSubscriber',
    'as' => 'delete-subscriber'
]);

Route::get('/page/add-page', 'PageController@index');

Route::post('/page/save', [
    'uses' => 'PageController@savePage',
    'as' => 'new-page'
]);

Route::get('/page/manage-page', [
    'uses' => 'PageController@managePage',
    'as' => 'manage-page'
]);

Route::get('/page/unpublished/{id}', [
    'uses' => 'PageController@unpublished',
    'as' => 'page-unpublished'
]);

Route::get('/page/published/{id}', [
    'uses' => 'PageController@published',
    'as' => 'page-published'
]);


Route::get('/page/edit/{id}', [
    'uses' => 'PageController@editPage',
    'as' => 'edit-page'
]);

Route::post('/page/update', [
    'uses' => 'PageController@updatePage',
    'as' => 'update-page'
]);

Route::get('/page/delete/{id}', [
    'uses' => 'PageController@deletePage',
    'as' => 'delete-page'
]);

Route::get('/page/show/{id}', [
    'uses' => 'PageController@showPage',
    'as' => 'show-page'
]);

Route::get('/order/manage-order', [
    'uses' => 'OrderController@mangeOrderInfo',
    'as' => 'manage-order'
]);

Route::get('order/view-order-detail/{id}', [
    'uses' => 'OrderController@viewOrderDetail',
    'as' => 'view-order-detail'
]);

Route::get('order/view-order-invoice/{id}', [
    'uses' => 'OrderController@viewOrderInvoice',
    'as' => 'view-order-invoice'
]);

Route::get('order/download-order-invoice/{id}', [
    'uses' => 'OrderController@downloadOrderInvoice',
    'as' => 'download-order-invoice'
]);





Route::get('/category-product/{id}', [
    'uses' => 'ShopController@categoryProduct',
    'as' => 'category-product'
]);

Route::get('/product-details/{id}', [
    'uses' => 'ShopController@productDetails',
    'as' => 'product-details'
]);

Route::post('/product-details/{id}', [
    'uses' => 'ShopController@coustomerReview',
    'as' => 'product-review'
]);


Route::post('/cart/add/', [
    'uses' => 'CartController@addToCart',
    'as' => 'add-to-cart'
]);

Route::get('/cart/show', [
    'uses' => 'CartController@showCart',
    'as' => 'show-cart'
]);

Route::get('/cart/delete/{id}', [
    'uses' => 'CartController@deleteCart',
    'as' => 'delete-cart-item'
]);

Route::post('/cart/update', [
    'uses' => 'CartController@updateCart',
    'as' => 'update'
]);

Route::get('/checkout', [
    'uses' => 'CheckoutController@index',
    'as' => 'checkout'
]);

Route::post('/checkout', [
    'uses' => 'CheckoutController@saveShippingInfo',
    'as' => 'new-shipping'
]);

Route::get('/complete/order', [
    'uses' => 'CheckoutController@completeOrder',
    'as' => 'complete-order'
]);

Route::get('/customer/login', [
    'uses' => 'CustomerController@loginIndex',
    'as' => '/'
]);

Route::post('/customer/login', [
    'uses' => 'CustomerController@customerLoginCheck',
    'as' => 'customer.login'
]);

//Route::post('/customer/logout', [
//    'uses' => 'CustomerController@customerLogout',
//    'as' => 'customer-logout'
//]);



Route::get('/customer/registration', [
    'uses' => 'CustomerController@coustomerIndex',
    'as' => '/'
]);

Route::post('/customer/registration/save', [
    'uses' => 'CustomerController@coustomerSingUp',
    'as' => 'customer.register'
]);






//Auth::routes();
//
//Route::resource('/slider', 'SliderController');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

