@extends ('front-end.master')

@section('body')

    <main>
        <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
            <div class="tt-layout__content">
                <div class="container">
                    <div class="tt-page__breadcrumbs">
                        <ul class="tt-breadcrumbs">
                            <li><a href="index.html"><i class="icon-home"></i></a></li>
                            <li><a href="index.html?page=listing-collections.html">Collections</a></li>
                            <li><span>Audio</span></li>
                        </ul>
                    </div>

                    <div class="tt-page__name text-center">
                        <h1>Contacts</h1>
                        <p>Dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
                    </div>

                    <div class="tt-contacts">
                        <div class="tt-contacts__adress">
                            <div class="row ttg-grid-padding--none">
                                <div class="col-md-6">
                                    <div class="tt-contacts__map">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d179203.69595995915!2d12.3000326!3d45.4283372!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sua!4v1496761644226"
                                                frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="tt-contacts__info">
                                        <div class="tt-contacts__info_text text-center">
                                            <p>Address: 7563 St. Vicent Place, Glasgow</p>
                                            <p>Phone: +777 2345 7885: +777 2345 7886</p>
                                            <p>Hours : 7 Days a week from 10-00 am to 6-00 pm</p>
                                            <p>E-mail: <a href="mailto:info@mydomain.com">info@mydomain.com</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="tt-page__cont-small">
                                <div class="tt-contacts__form tt-form">
                                    <div class="tt-contacts__form_title tt-form__title tt-form__title--lg text-center">Get
                                        in Touch
                                    </div>
                                    <p class="ttg__required text-center">Your email address will not be published. Required
                                        fields are marked</p>
                                    <div class="tt-contacts__form_inputs tt-form__form">
                                        <label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <span class="ttg__required">Name:</span>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control colorize-theme6-bg" placeholder="Enter your name" required="required">
                                                </div>
                                            </div>
                                        </label>
                                        <label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <span class="ttg__required">E-mail:</span>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control colorize-theme6-bg" placeholder="John.smith@example.com" required="required">
                                                </div>
                                            </div>
                                        </label>
                                        <label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <span class="ttg__required">Message:</span>
                                                </div>
                                                <div class="col-md-10">
                                                    <textarea class="form-control colorize-theme6-bg">Wtite your message here</textarea>
                                                </div>
                                            </div>
                                        </label>
                                        <div class="row">
                                            <div class="offset-md-2 col-md-10 text-center">
                                                <button class="btn btn--xs-flw">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tt-add-to-cart" data-active="true">
            <i class="icon-check"></i>
            <p>Added to Cart Successfully!</p>
            <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
        </div>

        <div class="tt-newsletter-popup" data-active="true">
            <div class="tt-newsletter-popup__text-01">
                <span>15</span>
                <span>
            <span>%</span>
            <span>off</span>
        </span>
            </div>
            <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
            <p>By signing up, you accept the terms & Privacy Policy.</p>
            <div class="ttg-mb--30">
                <form action="#" class="tt-newsletter tt-newsletter--style-02">
                    <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                    <button type="submit" class="btn">
                        <i class="tt-newsletter__text-wait"></i>
                        <span class="tt-newsletter__text-default">Subscribe!</span>
                        <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                        <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                    </button>
                </form>
            </div>
            <div class="tt-newsletter-popup__social">
                <div class="tt-social-icons tt-social-icons--style-03">
                    <a href="#" class="tt-btn">
                        <i class="icon-facebook"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-twitter"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-gplus"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-instagram-1"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-youtube-play"></i>
                    </a>
                </div>
            </div>
            <label class="tt-newsletter-popup__show_popup tt-checkbox">
                <input type="checkbox" name="show-nawslatter">
                <span></span>
                Don't show this popup again
            </label>
        </div>
    </main>
@endsection

