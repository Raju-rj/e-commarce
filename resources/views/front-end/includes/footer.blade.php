<footer>
    <div class="tt-footer tt-footer__01 ">
        <div class="tt-footer__content">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <a href="index.html" class="tt-logo">
                            <img src="images/logo.png" alt="Image name">
                        </a>
                    </div>
                    <div class="col-lg-6">
                        <div class="tt-footer__list-menu">
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul>
                                        @foreach($newPages as  $newPage)
                                            <li><a href="{{ route('show-page', ['id'=>$newPage->id]) }}">{{ $newPage->page_title }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>

                                {{--<div class="col-sm-4">--}}
                                {{--<ul>--}}
                                {{--<li><a href="#">Shipping & Returns</a></li>--}}
                                {{--<li><a href="#">Secure Shopping</a></li>--}}
                                {{--<li><a href="#">International Shipping</a></li>--}}
                                {{--<li><a href="#">Affiliates</a></li>--}}
                                {{--<li><a href="#">Group Sales</a></li>--}}
                                {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-4">--}}
                                {{--<ul>--}}
                                {{--<li><a href="#">Sign In</a></li>--}}
                                {{--<li><a href="#">View Cart</a></li>--}}
                                {{--<li><a href="#">My Wishlist</a></li>--}}
                                {{--<li><a href="#">Track My Order</a></li>--}}
                                {{--<li><a href="#">Help</a></li>--}}
                                {{--</ul>--}}
                                {{--</div>--}}

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <span class="tt-footer__title">Newsletter Signup</span>
                        <div class="tt-footer__newsletter">
                            <p>Sign up for our e-mail and be the first who know our special offers! Furthermore, we will
                                give a <span>15% discount</span> on the next order after you sign up.</p>
                            <form action="{{ route('new-email') }}" method="post" class="form-horizontal">
                                {{ csrf_field() }}
                                <input type="email" name="email" class="form-control"
                                       placeholder="Enter please your e-mail">
                                <button type="submit" class="btn">Subscribe
                                    {{--<i class="tt-newsletter__text-wait"></i>--}}
                                    {{--<span class="tt-newsletter__text-default">Subscribe!</span>--}}
                                    {{--<span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>--}}
                                    {{--<span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>--}}
                                </button>
                            </form>
                        </div>
                        <div class="tt-footer__social">
                            <div class="tt-social-icons tt-social-icons--style-01">
                                <a href="#" class="tt-btn">
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-gplus"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-instagram-1"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-youtube-play"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-pinterest"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-skype"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-behance"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <span class="tt-footer__copyright">&copy; 2018 . All Rights Reserved.</span>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="tt-footer__to-top tt-footer__to-top-desktop">
            <i class="icon-up-open-1"></i>
            <i class="icon-up"></i><span>Top</span>
        </a>
    </div>
</footer>