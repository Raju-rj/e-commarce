<header>
    <div class="tt-preloader"></div>

    <div class="tt-header tt-header--build-01 tt-header--style-01 tt-header--sticky">
        <div class="tt-header__content">
            <div class="">
                <div class="">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('/') }}invoice/images/logo.png" style="padding-top: 0px; margin-left: 0px; width: 220px" alt="mogo">
                    </a>
                </div>
            </div>
            <div class="tt-header__nav">
                <div class="tt-header__menu">
                    <nav class="TonyM TonyM--header"
                         data-tm-dir="row"
                         data-tm-mob="true"
                         data-tm-anm="emersion">
                        <ul class="TonyM__panel">

                            <li>
                                <a href="{{ url('/') }}">
                                   Home
                                    {{--<i class="TonyM__arw"></i>--}}
                                </a>

                            </li>

                            @foreach($newCategories as $newCategory)
                            <li>
                                <a href="{{ route('category-product', ['id'=>$newCategory->id]) }}">
                                   {{ $newCategory->category_name }}
                                    {{--<i class="TonyM__arw"></i>--}}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
                <div class="tt-header__sidebar">
                    <div class="tt-header__options">
                        <a href="#" class="tt-header__btn tt-header__btn-menu"><i class="icon-menu"></i></a>
                        <div role="search" class="tt-header__search">
                            <form action="#" class="tt-header__search-form">
                                <input type="search" name="q" class="form-control" placeholder="Search...">
                            </form>
                            <div class="tt-header__search-dropdown"></div>
                            <a href="#" class="tt-header__btn tt-header__btn-open-search"><i class="icon-search"></i></a>
                            <a href="#" class="tt-header__btn tt-header__btn-close-search"><i class="icon-cancel-1"></i></a>
                        </div>
                        <div>
                            <a href="#" class="tt-header__btn tt-header__btn-user"><i class="icon-user-outline"></i></a>
                            <div class="tt-header__user">
                                <ul class="tt-list-toggle">
                                    @if(Auth::check())
                                        <li><a href="#">My account</a></li>
                                        <li><a href="#"><span style="color: orangered">{{Auth::user()->first_name.' '.Auth::user()->last_name }}</span></a></li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <span>Logout</span>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    @else
                                        <li><a href="{{ url('customer/login') }}">Login</a></li>

                                        <li><a href="{{ url('customer/registration')  }}">Register</a></li>

                                        <div class="tt-header__login">
                                            <h6>Login</h6>
                                            <form action="{{ route('customer.login') }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="email" name="email" class="form-control" placeholder="Email" required="required">
                                                <input type="password" name="password" class="form-control" placeholder="Password" required="required">
                                                <b class="text-danger">{{session('message')}}</b>
                                                <button type="submit" name="btn" class="btn">Login</button>
                                                <div>
                                                    <a href="account.html">Forget your password?</a>
                                                </div>
                                            </form>
                                        </div>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <a href="wishlist.html" class="tt-header__btn tt-header__btn-wishlist">
                            <i class="icon-heart-empty-2"></i>
                            <span>1</span>
                        </a>
                        <div>
                            <a href="cart.html" class="tt-header__btn tt-header__btn-cart">
                                <i class="icon-shop24"></i>
                                <span>{{ count($cartProducts) }}</span>
                            </a>
                            <div class="tt-header__cart">
                                <div class="tt-header__cart-content">

                                    @foreach($cartProducts as $cartProduct)

                                        <ul class="colorize-bd">
                                            <li>
                                                <div>
                                                    <a href="{{ url('product-details', ['id'=>$cartProduct->id]) }}">
                                                        <img src="{{ asset($cartProduct->options->image) }}" alt="image" class="colorize-theme2-bd colorize-theme-bd-h">
                                                    </a>
                                                </div>
                                                <div>
                                                    <p>
                                                        <a href="{{ url('product-details', ['id'=>$cartProduct->id]) }}">{{ $cartProduct->name }}</a>
                                                    </p>
                                                    <span class="tt-header__cart-price">
                                                        <span class="tt-header__cart-price-count">{{ $cartProduct->qty }}</span>
                                                        <span>x</span>
                                                        <span class="tt-header__cart-price-val tt-price">{{ $cartProduct->price }}</span>
                                                    </span>
                                                </div>
                                                <div>
                                                    <a href="{{ url('/cart/show') }}" class="tt-header__cart-edit"><i class="icon-pencil-circled"></i></a>
                                                    <a href="{{ route('delete-cart-item', ['rowId'=>$cartProduct->rowId]) }}" class="tt-header__cart-delete" title="remove"><i class="icon-trash"></i></a>
                                                </div>
                                            </li>
                                        </ul>
                                    @endforeach
                                    <div class="tt-header__cart-footer">
                                        <span class="tt-header__cart-subtotal colorize-head-c">Subtotal:
                                            <span class="colorize-theme-c">BDT. {{ Session::get('orderTotal') }}</span>
                                        </span>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <a href="{{ url('/cart/show') }}" class="tt-header__cart-viewcart btn"><i class="icon-shop24"></i> View Cart</a>
                                            </div>
                                            <div class="col-lg-6">
                                                <a href="{{ url('/checkout') }}" class="tt-header__cart-checkout btn colorize-btn2"><i class="icon-check"></i> Checkout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="#" class="tt-header__btn tt-header__btn-settings"><i class="icon-cog"></i></a>
                            <div class="tt-header__settings">
                                <ul class="tt-list-toggle">
                                    <li class="tt-list-toggle__open"><a href="#">language: USA</a>
                                        <ul>
                                            <li><a href="#">POL</a></li>
                                            <li class="active"><a href="#">USA</a></li>
                                            <li><a href="#">RUS</a></li>
                                        </ul>
                                    </li>
                                    <li class="tt-list-toggle__open"><a href="#">Currency: <span>USD</span></a>
                                        <ul id="currencies">
                                            <li class="active"><a href="#">USD</a></li>
                                            <li><a href="#">RUB</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>