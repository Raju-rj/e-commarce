@extends('front-end.master')

@section('title')
    Registraion
@endsection

@section('body')


<main>
    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
               <div class="col-md-12">
                <div class="tt-page__breadcrumbs">
                    <ul class="tt-breadcrumbs">
                        <li><a href="index.html"><i class="icon-home"></i></a></li>
                        <li><a href="index.html?page=listing-collections.html">Collections</a></li>
                        <li><span>Audio</span></li>
                    </ul>
                </div>

                <div class="tt-page__name text-center">
                    <a href="index.html?page=login.html">Login</a>
                    <span>/</span>
                    <h1>Register</h1>
                </div>

                <div class="tt-login">
                    <div class="tt-login__title">
                        <p>Personal Information</p>
                    </div>
                    <form method="POST" action="{{ route('customer.register') }}">
                        @csrf
                        <div class="tt-checkout__form">
                            <div class="row">

                                <div class="col-md-3"><p class="ttg__required">First Name</p></div>

                                <div class="col-md-9">
                                    <div class="tt-input">
                                        <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }} colorize-theme6-bg" name="first_name" value="{{ old('first_name') }}" required autofocus>
                                        {{--<input type="text" name="first_name" class="form-control colorize-theme6-bg" placeholder="Enter please your first name">--}}
                                        {{--<span class="text-danger">{{$errors->has('first_name') ? $errors->first('first_name') : ' ' }}</span>--}}
                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-3"><p class="ttg__required">Last Name</p></div>
                                <div class="col-md-9">
                                    <div class="tt-input">
                                        <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }} colorize-theme6-bg" name="last_name" value="{{ old('last_name') }}" required autofocus>
                                        {{--<input type="text" name="last_name" class="form-control colorize-theme6-bg" placeholder="Enter please your last name">--}}
                                        {{--<span class="text-danger">{{$errors->has('last_name') ? $errors->first('last_name') : ' ' }}</span>--}}
                                        @if ($errors->has('last_name'))
                                            <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-3"><p class="ttg__required">E-Mail Address</p></div>

                                <div class="col-md-9">
                                    <div class="tt-input">
                                        <input id="email_address" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} colorize-theme6-bg" name="email" value="{{ old('email') }}" required>
                                        {{--<input type="text" name="email_address" class="form-control colorize-theme6-bg" placeholder="exmple@email.com">--}}
                                        {{--<span class="text-danger">{{$errors->has('email_address') ? $errors->first('email_address') : ' ' }}</span>--}}
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-3"><p class="ttg__required">Password</p></div>
                                <div class="col-md-9">
                                    <div class="tt-input">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} colorize-theme6-bg" name="password" required>
                                        {{--<input type="password" name="password" class="form-control colorize-theme6-bg" placeholder="Enter please your password">--}}
                                        {{--<span class="text-danger">{{$errors->has('password') ? $errors->first('password') : ' ' }}</span>--}}
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                          </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-3"><p class="ttg__required">Confirm Password</p></div>
                                <div class="col-md-9">
                                    <div class="tt-input">
                                        <input id="password-confirm" type="password" class="form-control colorize-theme6-bg" name="password_confirmation">
                                        {{--<input type="number" name="phone_number" class="form-control colorize-theme6-bg" placeholder="Enter please your phone number">--}}
                                        <span class="text-danger">{{$errors->has('phone_number') ? $errors->first('phone_number') : ' ' }}</span>
                                    </div>
                                </div>


                                <div class="col-md-3"><p class="ttg__required">Phone Number</p></div>
                                <div class="col-md-9">
                                    <div class="tt-input">
                                        <input id="phone_number" type="number" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }} colorize-theme6-bg" name="phone_number" value="{{ old('phone_number') }}" required autofocus>
                                        {{--<input type="number" name="phone_number" class="form-control colorize-theme6-bg" placeholder="Enter please your phone number">--}}
                                        {{--<span class="text-danger">{{$errors->has('phone_number') ? $errors->first('phone_number') : ' ' }}</span>--}}
                                        @if ($errors->has('phone_number'))
                                            <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-3"><p class="ttg__required">Address</p></div>
                                <div class="col-md-9">
                                    <div class="tt-input">
                                    <textarea id="address" type="number" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }} colorize-theme6-bg" name="address"  required autofocus>{{ old('address') }}</textarea>
                                        {{--<input type="number" name="phone_number" class="form-control colorize-theme6-bg" placeholder="Enter please your phone number">--}}
                                        {{--<span class="text-danger">{{$errors->has('phone_number') ? $errors->first('phone_number') : ' ' }}</span>--}}
                                        @if ($errors->has('address'))
                                            <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
.





                                {{--<div class="col-md-3"><p class="ttg__required">Address</p></div>--}}
                                {{--<div class="col-md-9">--}}
                                    {{--<div class="tt-input">--}}

                                        {{--<textarea type="text" name="address" class="form-control colorize-theme6-bg" placeholder="Enter please your address"></textarea>--}}
                                        {{--<span class="text-danger">{{$errors->has('address') ? $errors->first('address') : ' ' }}</span>--}}
                                        {{--@if ($errors->has('address'))--}}
                                            {{--<span class="invalid-feedback text-danger">--}}
                                            {{--<strong>{{ $errors->first('address') }}</strong>--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-9 offset-md-3">
                                <div class="">
                                    <button type="submit" name="btn" class="tt-checkout__btn-order btn btn-type--icon colorize-btn6"><i class="icon-check"></i><span>Create</span></button>
                                </div>
                                <div class="tt-login__tostore ttg-mt--0 text-center"><br>
                                <span>or</span><br>
                                <a href="#">Return to Store</a>
                                </div>
                            </div>

                            {{--<div class="col-md-9 offset-md-3 ttg-mt--40">--}}
                                {{--<button type="submit" class="btn">Create</button>--}}
                                {{--<div class="tt-login__tostore">--}}
                                    {{--<span>or</span>--}}
                                    {{--<a href="#">Return to Store</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection