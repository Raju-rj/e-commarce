@extends('front-end.master')

@section('title')
    Login
@endsection

@section('body')


    <main>
    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                <div class="tt-page__breadcrumbs">
                    <ul class="tt-breadcrumbs">
                        <li><a href="index.html"><i class="icon-home"></i></a></li>
                        <li><a href="listing-collections.html">Collections</a></li>
                        <li><span>Audio</span></li>
                    </ul>
                </div>

                <div class="tt-page__name text-center">
                    <h1>Login</h1>
                    <span>/</span>
                    <a href="register.html">Register</a><br>
                </div>

                {{--<h4 class="text text-center text-danger">{{session('error')}}</h4>--}}
                <h4 class="text-center text-success">{{session('registed')}}</h4>

                <div class="tt-login">
                    <h6 class="text text-center text-danger">{{session('error')}}</h6>

                    <form action="{{ route('customer.login') }}" method="post">
                        {{ csrf_field() }}
                        <div class="tt-form">
                            <div class="tt-checkout__form">
                                <div class="row">
                                    <div class="col-md-3"><p class="ttg__required">Email Address:</p></div>
                                    <div class="col-md-9">
                                        <div class="tt-input">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} colorize-theme6-bg" placeholder="exmple@email.com" name="email" value="{{ old('email') }}" autofocus>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback text-danger">
                                                   <strong>{{ $errors->first('email') }}</strong>
                                                 </span>
                                            @endif
                                            {{--<input type="text" name="email" class="form-control colorize-theme6-bg" placeholder="exmple@email.com">--}}
                                        </div>
                                    </div>
                                    <div class="col-md-3"><p class="ttg__required">Password:</p></div>
                                    <div class="col-md-9">
                                        <div class="tt-input">
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} colorize-theme6-bg" name="password" placeholder="Enter please your password">

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback text-danger">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                 </span>
                                            @endif
                                            {{--<input type="password" name="password" class="form-control colorize-theme6-bg" placeholder="Enter please your password">--}}
                                            <b class="text-danger">{{session('message')}}</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 col-sm-6 offset-md-3">
                            <label class="tt-checkbox">
                                <input type="checkbox">
                                <span></span>
                                <p>Remember me</p>
                            </label>
                        </div>

                        <div class="row ttg-mt--20">
                            <div class="col-md-9 offset-md-3">
                                <button name="log-in" type="submit" class="btn">Login</button>
                            </div>
                        </div>
                        <div class="row ttg-mt--20">
                            <div class="col-md-6 col-sm-8 text-right" style="margin-left: 20px "><a href="#">Lost your password?</a></div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection