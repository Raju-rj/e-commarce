@extends('front-end.master')

@section('title')
    Shopping Cart
@endsection

@section('body')

 <main>
        <div class="tt-layout tt-sticky-block__parent ">
            <div class="tt-layout__content">
                <div class="container">
                    <div class="tt-page__breadcrumbs">
                        <ul class="tt-breadcrumbs">
                            <li><a href="index.html"><i class="icon-home"></i></a></li>
                            <li><a href="index.html?page=listing-collections.html">Collections</a></li>
                            <li><span>Audio</span></li>
                        </ul>
                    </div>

                    <div class="tt-page__name text-center">
                        <h1>Shopping Cart</h1>
                    </div>

                    <div class="tt-cart">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tt-cart__caption">
                                    <div class="row">
                                        <div class="col-md-2"><span>Products</span></div>
                                        <div class="col-md-2"><span>Product Name</span></div>
                                        <div class="col-md-2 text-center"><span>Price</span></div>
                                        <div class="col-md-2 text-center"><span>Quantity</span></div>
                                        <div class="col-md-2 text-center"><span>Total</span></div>
                                        <div class="col-md-2 text-center"><span>Action</span></div>
                                    </div>
                                </div>
                                <div class="tt-cart__list" style="padding-top: 0px; padding-bottom: 30px;">

                                    @php($sum = 0)
                                    @foreach($cartProducts as $cartProduct)
                                      <form action="{{ route('update') }}" method="post">
                                            {{ csrf_field() }}
                                        <div class="tt-cart__product">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <a href="{{ route('delete-cart-item', ['rowId'=>$cartProduct->rowId]) }}" class="tt-cart__product_del"><i class="icon-trash"></i></a>
                                                    <a href="#" class="tt-cart__product_image"><img
                                                                src="{{ asset($cartProduct->options->image) }}" alt="Image name"></a>
                                                    <div class="tt-cart__product_info">
                                                        <a href="#"><p>{{ $cartProduct->name }}</p></a>
                                                        <p>Color: <span>Orange</span></p>
                                                        <p>Size: <span>XL</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-4 text-center">
                                                    <div class="tt-cart__product_price">
                                                        <div class="tt-price">
                                                            <span>BDT. {{ $cartProduct->price }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-4 text-center">
                                                    <div class="tt-counter tt-counter__inner" data-min="1" data-max="50">
                                                        <input type="number" name="qty" class="form-control"
                                                               value="{{ $cartProduct->qty }}" min="1" max="50">
                                                        <input type="hidden" name="rowId" class="form-control"
                                                               value="{{ $cartProduct->rowId }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-4 text-center">
                                                    <div class="tt-cart__product_price">
                                                        <div class="tt-price">
                                                            <span>BDT. {{ $total = $cartProduct->price * $cartProduct->qty }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-4 text-center">
                                                    <div class="tt-cart__product_price">
                                                        <div class="tt-price">
                                                            <button type="submit" name="btn" class="btn btn-type--icon colorize-btn6" title="Update Quantity"><i
                                                                        class="icon-arrows-cw-1"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       </form>
                                        <?php $sum = $sum + $total ?>
                                    @endforeach
                                </div>


                                {{--<div class="col-md-4 col-lg-offset-5">--}}
                                    {{--<table>--}}
                                            {{--<tr class="">--}}
                                                {{--<th>Subtotal: <span>$78</span></th>--}}
                                            {{--</tr>--}}
                                            {{--<hr/>--}}
                                            {{--<tr class="">--}}
                                                {{--<td>Total: <span>$78</span></td>--}}
                                            {{--</tr>--}}
                                        {{--<a href="$" class="tt-summary__btn-checkout btn btn-type--icon colorize-btn6"><i--}}
                                                    {{--class="icon-check"></i><span>Proeed to Checkout</span></a>--}}
                                    {{--</table>--}}
                                {{--</div>--}}
                                    {{--<script>--}}
                                        {{--require(['app'], function () {--}}
                                            {{--require(['modules/categories']);--}}
                                        {{--});--}}
                                    {{--</script>--}}

                                <table class="table table-bordered table-hover">
                                    <tr><th>Subtotal:</th><td class="text-right" style="font-size: 18px; color: black"><strong>BDT. {{ $sum }}</strong></td></tr>
                                    <tr><th>Shipping & Handling (Shipping Charge)</th><td class="text-right" style="font-size: 18px; color: black"><strong>BDT. 0</strong></td></tr>
                                    <tr><th>Grand Total</th><td class="text-right" style="font-size: 18px; color: black"><strong>BDT. {{ $orderTotal = $sum }}</strong></td></tr>
                                    <?php Session::put('orderTotal', $orderTotal) ?>
                                </table>

                                <div class="tt-cart__footer">
                                    <div class="row">
                                        <div class="col-md-6">
                                                <a href="{{ url('/') }}" type="submit" class="btn">Shop more</a>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <a href="{{ url('/checkout') }}" class="btn btn-type--icon colorize-btn6"><i class="icon-check"></i><span>Proeed to Checkout</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tt-add-to-cart" data-active="true">
            <i class="icon-check"></i>
            <p>Added to Cart Successfully!</p>
            <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
        </div>

        <div class="tt-newsletter-popup" data-active="true">
            <div class="tt-newsletter-popup__text-01">
                <span>15</span>
                <span>
            <span>%</span>
            <span>off</span>
        </span>
            </div>
            <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
            <p>By signing up, you accept the terms & Privacy Policy.</p>
            <div class="ttg-mb--30">
                <form action="#" class="tt-newsletter tt-newsletter--style-02">
                    <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                    <button type="submit" class="btn">
                        <i class="tt-newsletter__text-wait"></i>
                        <span class="tt-newsletter__text-default">Subscribe!</span>
                        <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                        <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                    </button>
                </form>
            </div>
            <div class="tt-newsletter-popup__social">
                <div class="tt-social-icons tt-social-icons--style-03">
                    <a href="#" class="tt-btn">
                        <i class="icon-facebook"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-twitter"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-gplus"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-instagram-1"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-youtube-play"></i>
                    </a>
                </div>
            </div>
            <label class="tt-newsletter-popup__show_popup tt-checkbox">
                <input type="checkbox" name="show-nawslatter">
                <span></span>
                Don't show this popup again
            </label>
        </div>
    </main>

@endsection