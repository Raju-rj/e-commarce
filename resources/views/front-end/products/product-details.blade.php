@extends('front-end.master')

@section('title')
    Product-details
@endsection

@section('body')
<main>
    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                <div class="tt-product-page">
                    <div class="tt-product-page__breadcrumbs">
                        <ul class="tt-breadcrumbs">
                            <li><a href="#"><i class="icon-home"></i></a></li>
                            <li><a href="#?page=listing-collections.html">Collections</a></li>
                            <li><span>Audio</span></li>
                        </ul>
                    </div>


                    <div class="tt-product-head tt-sticky-block__parent">
                        <div class="tt-product-head__sticky tt-sticky-block tt-layout__mobile-full">
                            <div class="tt-product-head__images tt-sticky-block__inner tt-product-head__single-mobile ">
                                <div class="tt-product-head__image-main">
                                    <img src="{{ asset($product->product_image) }}"
                                         data-zoom-image="{{ asset($product->product_image) }}" data-full="#"
                                         alt="Image name">
                                    <img src="images/products/product-02-2.jpg"
                                         data-zoom-image="images/products/product-02-2.jpg" data-full="#"
                                         alt="Image name">
                                    <img src="images/products/product-02-3.jpg"
                                         data-zoom-image="images/products/product-02-3.jpg" data-full="#"
                                         alt="Image name">
                                    <img src="images/products/product-09.jpg"
                                         data-zoom-image="images/products/product-09.jpg" data-full="#"
                                         alt="Image name">
                                    <img src="images/products/product-09-2.jpg"
                                         data-zoom-image="images/products/product-09-2.jpg" data-full="#"
                                         alt="Image name">
                                    <img src="images/products/product-09-3.jpg"
                                         data-zoom-image="images/products/product-09-3.jpg" data-full="#"
                                         alt="Image name">
                                    <a href="http://youtube.com/watch?v=C3lWwBslWqg"></a>
                                </div>
                                <div class="tt-product-head__image-preview">
                                    <img src="images/products/product-02.jpg" alt="Image name">
                                    <img src="images/products/product-02-2.jpg" alt="Image name">
                                    <img src="images/products/product-02-3.jpg" alt="Image name">
                                    <img src="images/products/product-09.jpg" alt="Image name">
                                    <img src="images/products/product-09-2.jpg" alt="Image name">
                                    <img src="images/products/product-09-3.jpg" alt="Image name">
                                    <span class="tt-product-head__btn-video"><i class="icon-video"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="tt-product-head__sticky tt-sticky-block">
                            <div class="tt-product-head__info tt-sticky-block__inner">
                                    <div class="tt-product-head__category"><span>{{ $product->product_name }}</span>
                                    </div>
                                    <div class="tt-product-head__name"><h3>{{ $product->short_description }}</h3></div>
                                    <div class="tt-product-head__review">
                                        <div class="tt-product-head__stars
                            tt-stars">
                                            <span class="ttg-icon"></span>
                                            <span class="ttg-icon" style="width: 70%;"></span>
                                        </div>
                                        <div class="tt-product-head__review-count"><a href="#">2 Review(s)</a></div>
                                        <div class="tt-product-head__review-add"><a href="#">Add Your Review</a></div>
                                    </div>
                                    <div class="tt-product-head__price">
                                        <div class="tt-price">
                                            <span>Tk.{{ $product->product_price }}</span>
                                        </div>
                                    </div>
                                    <div class="tt-product-head__more-detailed"><p>{{ $product->long_description }}</p></div>

                                    <form action="{{ route('add-to-cart') }}" method="post">
                                        {{ csrf_field() }}
                                    <div class="tt-product-head__control">
                                        <div class="tt-product-head__counter tt-counter tt-counter__inner">
                                            <input type="number" name="qty" class="form-control" value="1" min="1">
                                            <input type="hidden" name="id" value="{{ $product->id }}">
                                            <div class="tt-counter__control">
                                                {{--<span class="icon-up-circle" data-direction="next"></span>--}}
                                                {{--<span class="icon-down-circle" data-direction="prev"></span>--}}
                                            </div>
                                        </div>

                                        <button type="submit" name="btn" value="Add to Cart" class="product-head__cart tt-btn tt-btn--cart colorize-btn6"><i class="icon-shop24"></i>
                                            Add to Cart
                                        </button>

                                    </div>
                                    </form>

                                    <div class="tt-product-head__tags"><span>Tags:</span>
                                        <a href="#">Wireless</a>,
                                        <a href="#">Built-In Microphone</a>,
                                        <a href="#">Bluetooth Enabled</a>
                                    </div>
                                    <div class="addthis_inline_share_toolbox"></div>
                                    <div class="tt-product-head__video tt-video">
                                        <!--<video src="media/video-01.mp4" controls="controls"></video>-->
                                        <iframe src="https://www.youtube.com/embed/AoPiLg8DZ3A" frameborder="0"
                                                allowfullscreen></iframe>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="tt-product-page__tabs tt-tabs tt-layout__mobile-full" data-tt-type="horizontal">
                        <div class="tt-tabs__head">
                            <div class="tt-tabs__slider">
                                <div class="tt-tabs__btn" data-active="true"><span>Description</span></div>
                                <div class="tt-tabs__btn" data-tab="review"><span>Reviews</span></div>
                            </div>
                            <div class="tt-tabs__btn-prev"></div>
                            <div class="tt-tabs__btn-next"></div>
                            <div class="tt-tabs__border"></div>
                        </div>
                        <div class="tt-tabs__body tt-tabs-product">
                            <div>
                                <span>Description <i class="icon-down-open"></i></span>
                                <div class="tt-tabs__content">
                                    <div class="tt-tabs__content-head">Description</div>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                        veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                                        voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                                    <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam
                                        rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
                                        beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                    <ul class="tt-tabs-product__list">
                                        <li><span>Omnis iste natus error sit voluptatem</span></li>
                                        <li><span>Accusantium doloremque</span></li>
                                        <li><span>Laudantium, totam rem aperiam,</span></li>
                                        <li><span>Eaque ipsa quae ab illo inventore veritatis</span></li>
                                    </ul>
                                </div>
                            </div>

                            <div>
                                <span>Reviews <i class="icon-down-open"></i></span>
                                <div class="tt-tabs__content">
                                    <div class="tt-tabs__content-head">Customer Reviews</div>
                                    <div class="tt-tabs-product__review tt-review">
                                        <div class="tt-review__head">
                                            <div class="tt-review__head-stars tt-stars">
                                                <span class="ttg-icon"></span>
                                                <span class="ttg-icon" style="width: 70%;"></span>
                                            </div>
                                            <span>Based on 2 review</span>
                                            <a href="#">Write a review</a>
                                        </div>
                                        <div class="tt-review__form">
                                            <span>Write a review</span>
                                            <form action="{{ route('product-review', ['id'=>$product->id]) }}" method="post">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-sm-4"><label for="reviewName">Name:</label></div>
                                                    <div class="col-sm-8">
                                                        <input name="name" type="text" id="reviewName" class="form-control"
                                                               placeholder="Enter your name">
                                                    </div>
                                                </div>
                                                <div class="row ttg-mt--20">
                                                    <div class="col-sm-4"><label for="reviewEmail">E-mail:</label></div>
                                                    <div class="col-sm-8">
                                                        <input name="email_address" type="text" id="reviewEmail" class="form-control"
                                                               placeholder="John.smith@example.com">
                                                    </div>
                                                </div>
                                                <div class="row ttg-mt--20">
                                                    <div class="col-sm-4"><label for="reviewTitle">Review Title:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input name="review_title" type="text" id="reviewTitle" class="form-control"
                                                               placeholder="Give your review a title">
                                                    </div>
                                                </div>
                                                <div class="row ttg-mt--20">
                                                    <div class="col-sm-4"><label for="reviewBody">Body of Review
                                                            (1500):</label></div>
                                                    <div class="col-sm-8">
                                                        <textarea name="comments" id="reviewBody" class="form-control" placeholder="Wtite your comments here"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row ttg-mt--20">
                                                    <div class="col-sm-8 offset-sm-4">
                                                        <button type="submit" class="btn">Submit Review</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tt-review__comments">

                                            @foreach($reviews as $review)
                                            <div>
                                                <div class="tt-stars">
                                                    <span class="ttg-icon"></span>
                                                    <span class="ttg-icon" style="width: 70%;"></span>
                                                </div>
                                                <div class="tt-review__comments-title">{{ $review->review_title }}</div>
                                                <span><span>{{ $review->name }} on</span> {{ $review->created_at }}</span>
                                                <p>{{ $review->comments }}</p>
                                            </div>
                                             <hr>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tt-product-page__upsell">
                        <div class="tt-product-page__upsell-title">You may also be interested in the follwing
                            product(s)
                        </div>
                        <div class="tt-carousel-box">
                            <div class="tt-product-view">
                                <div class="tt-carousel-box__slider">
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-01.jpg"
                                                         data-retina="images/products/product-01.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                                <div class="tt-product__labels">
                                                    <span class="tt-label__new">New</span>
                                                    <span class="tt-label__hot">Hot</span>

                                                    <span class="tt-label__sale">Sale</span>
                                                    <span class="tt-label__discount">$22</span>

                                                    <div><span class="tt-label__out-stock">Out Stock</span></div>
                                                    <div><span class="tt-label__in-stock">In Stock</span></div>
                                                </div>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Headphones</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$22</span>
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                                    </div>

                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__stars tt-stars">
                                                            <span class="ttg-icon"></span>
                                                            <span class="ttg-icon" style="width:86%;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart tt-btn__state--active">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like tt-btn__state--active">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare tt-btn__state--active">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-02.jpg"
                                                         data-retina="images/products/product-02.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Phone</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$46</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__stars tt-stars">
                                                            <span class="ttg-icon"></span>
                                                            <span class="ttg-icon" style="width:35%;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-03.jpg"
                                                         data-retina="images/products/product-03.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                                <div class="tt-product__labels">
                                                    <span class="tt-label__sale">Sale</span>
                                                    <span class="tt-label__discount">$32</span>
                                                </div>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">USB</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$32</span>
                                                    <span>$38</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-04.jpg"
                                                         data-retina="images/products/product-04.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$14</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-05.jpg"
                                                         data-retina="images/products/product-05.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$24</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-06.jpg"
                                                         data-retina="images/products/product-06.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-07.jpg"
                                                         data-retina="images/products/product-07.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$58</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-08.jpg"
                                                         data-retina="images/products/product-08.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$19</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Product",
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "3.5",
    "reviewCount": "11"
  },
  "description": "0.7 cubic feet countertop microwave. Has six preset cooking categories and convenience features like Add-A-Minute and Child Lock.",
  "name": "Kenmore White 17\" Microwave",
  "image": "kenmore-microwave-17in.jpg",
  "offers": {
    "@type": "Offer",
    "availability": "http://schema.org/InStock",
    "price": "55.00",
    "priceCurrency": "USD"
  },
  "review": [
    {
      "@type": "Review",
      "author": "Ellie",
      "datePublished": "2011-04-01",
      "description": "The lamp burned out and now I have to replace it.",
      "name": "Not a happy camper",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "1",
        "worstRating": "1"
      }
    },
    {
      "@type": "Review",
      "author": "Lucas",
      "datePublished": "2011-03-25",
      "description": "Great microwave for the price. It is small and fits in my apartment.",
      "name": "Value purchase",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "4",
        "worstRating": "1"
      }
    }
  ]
}
                </script>
            </div>
        </div>
    </div>

</main>
@endsection