@extends('front-end.master')

@section('title')
    Home
@endsection

@section('body')

    <main>
        <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
            <div class="tt-layout__content">
                <div class="container">
                    <div class="tt-sr" data-layout="fullscreen">
                        <div class="tt-sr__content" data-version="5.3.0.2">
                            <ul>

                             @foreach($newSliders as  $newSlider)

                                <li data-index="rs-3045"
                                    data-transition="parallaxvertical"
                                    data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                                    data-masterspeed="1500">
                                    <img src="{{ asset($newSlider->slider_image) }}"
                                         alt="Image name"
                                         class="rev-slidebg"
                                         data-bgposition="center center"
                                         data-bgfit="cover"
                                         data-bgrepeat="no-repeat"
                                         data-bgparallax="8">
                                    <div class="tp-caption
                                rs-parallaxlevel-3
                                tt-sr__text"
                                         data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[-100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                         data-x="left"
                                         data-y="center"
                                         data-whitespace="nowrap"
                                         data-width="['auto']"
                                         data-height="['auto']"
                                         data-hoffset="76">
                                        <div>Speaker Bottle</div>
                                        <span>$121</span>
                                        <p>Portable Bluetooth stereo speakers</p>
                                        <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                            <i class="icon-shop24"></i>
                                        </a>
                                    </div>
                                </li>

                              @endforeach

                            </ul>
                        </div>
                    </div>

                    <div class="tt-home__promobox-01">
                        <div class="container-fluid ttg-cont-padding--none">
                            <div class="row ttg-grid-padding--none">
                                <div class="col-sm-8">
                                    <div class="row ttg-grid-padding--none">
                                        <div class="col-sm-6">
                                            <a href="#" class="tt-promobox
                                                           ttg-text-animation-parent
                                                           ttg-image-translate--right
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md"
                                            >
                                                <div class="tt-promobox__content">
                                                    <img src="#" data-srcset="images/promoboxes/promobox-01.jpg"
                                                         alt="Image name">
                                                    <div class="tt-promobox__text"
                                                         data-resp-md="md"
                                                         data-resp-sm="md"
                                                         data-resp-xs="sm">
                                                        <div>Watches</div>
                                                    </div>
                                                    <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                        <div class="tt-promobox__hover-bg"></div>
                                                        <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                            <div class="ttg-text-animation--emersion">
                                                                <span>Watches</span>
                                                            </div>
                                                            <p class="ttg-text-animation--emersion">
                                                                <span><span>28</span> products</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6">
                                            <a href="#" class="tt-promobox
                                                           ttg-text-animation-parent
                                                           ttg-image-translate--top
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                                <div class="tt-promobox__content">
                                                    <img src="#" data-srcset="images/promoboxes/promobox-02.jpg"
                                                         alt="Image name">
                                                    <div class="tt-promobox__text"
                                                         data-resp-md="md"
                                                         data-resp-sm="md"
                                                         data-resp-xs="sm">
                                                        <div class="colorize-theme2-c">Trackers</div>
                                                    </div>
                                                    <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                        <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                                        <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                            <div class="ttg-text-animation--emersion">
                                                                <span class="colorize-theme2-c">Trackers</span>
                                                            </div>
                                                            <p class="ttg-text-animation--emersion">
                                                                <span class="colorize-theme2-c"><span>46</span> products</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-12">
                                            <a href="#" class="tt-promobox
                                                           tt-promobox__size-wide
                                                           ttg-text-animation-parent
                                                           ttg-image-scale
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                                <div class="tt-promobox__content">
                                                    <img src="#" data-srcset="images/promoboxes/promobox-03.jpg"
                                                         alt="Image name">
                                                    <div class="tt-promobox__text"
                                                         data-resp-md="md"
                                                         data-resp-sm="md"
                                                         data-resp-xs="sm">
                                                        <div class="colorize-theme2-c">Headphones</div>
                                                    </div>
                                                    <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                        <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                                        <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                            <div class="ttg-text-animation--emersion">
                                                                <span class="colorize-theme2-c">Headphones</span>
                                                            </div>
                                                            <p class="ttg-text-animation--emersion">
                                                                <span class="colorize-theme2-c">32 products</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <a href="#" class="tt-promobox
                                                   tt-promobox__size-high
                                                   ttg-text-animation-parent
                                                   ttg-image-translate--left
                                                   ttg-animation-disable--md
                                                   tt-promobox__hover-disable--md">
                                        <div class="tt-promobox__content">
                                            <img src="#" data-srcset="images/promoboxes/promobox-04.jpg" alt="Image name">
                                            <div class="tt-promobox__text"
                                                 data-resp-md="md"
                                                 data-resp-sm="md"
                                                 data-resp-xs="sm">
                                                <div class="colorize-theme2-c">Earphones</div>
                                            </div>
                                            <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                <div class="tt-promobox__hover-bg colorize-theme-bg"></div>
                                                <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                    <div class="ttg-text-animation--emersion">
                                                        <span class="colorize-theme2-c">Earphones</span>
                                                    </div>
                                                    <p class="ttg-text-animation--emersion">
                                                        <span class="colorize-theme2-c">24 products</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid ttg-cont-padding--none">
                        <div class="row tt-product-view ttg-grid-padding--none">

                            @foreach($newProducts as $newProduct)
                            <div class="col-sm-6 col-xl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="{{ asset($newProduct->product_image) }}" data-srcset="{{ asset($newProduct->product_image) }}"
                                                 data-retina="{{ asset($newProduct->product_image) }}"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">{{ $newProduct->product_name }}</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">{{ $newProduct->short_description }}</a>
                                            </span>
                                            </p>

                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>Tk. {{ $newProduct->product_price }}</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__stars tt-stars">
                                                    <span class="ttg-icon"></span>
                                                    <span class="ttg-icon" style="width:35%;"></span>
                                                </div>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">

                                                        <a href="{{ route('product-details', ['id'=>$newProduct->id]) }}" class="tt-btn colorize-btn5 -product__buttons_cart">
                                                            <i class="icon-shop24"></i>
                                                        </a>

                                                        <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                            <i class="icon-heart-empty-2"></i>
                                                        </a>
                                                        <a href="{{ route('product-details', ['id'=>$newProduct->id]) }}" class="tt-btn colorize-btn4 __buttons_qv">
                                                            <i class="icon-eye"></i>
                                                        </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="tt-home__shipping-info-01">
                        <div class="tt-shp-info tt-shp-info__design-01">
                            <div class="row ttg-grid-padding--none ttg-grid-border">
                                <div class="col-lg-4">
                                    <a href="#" class="tt-shp-info__section ">
                                        <i class="icon-phone"></i>
                                        <div class="tt-shp-info__strong">+(777) 2345 7885</div>
                                        <p>Toll-free hotline. 7 days a week from <strong><em>10.00 a.m. to 6.00
                                                    p.m.</em></strong></p>
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#" class="tt-shp-info__section ">
                                        <i class="icon-box"></i>
                                        <div class="tt-shp-info__strong">Free Shipping</div>
                                        <p>Shipping prices for any form of delivery and order’s cost is constant - $49. A
                                            free shipping is available for orders <span>more than $99.</span></p>
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#" class="tt-shp-info__section ">
                                        <i class="icon-left"></i>
                                        <div class="tt-shp-info__strong">Returns and Exchanges</div>
                                        <p>Any goods, that was bought in our online store, can be returned during <span>30 days</span>
                                            since purchase date.</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tt-add-to-cart" data-active="true">
            <i class="icon-check"></i>
            <p>Added to Cart Successfully!</p>
            <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
        </div>

        <div class="tt-newsletter-popup" data-active="true">
            <div class="tt-newsletter-popup__text-01">
                <span>15</span>
                <span>
            <span>%</span>
            <span>off</span>
        </span>
            </div>
            <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
            <p>By signing up, you accept the terms & Privacy Policy.</p>
            <div class="ttg-mb--30">
                <form action="#" class="tt-newsletter tt-newsletter--style-02">
                    <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                    <button type="submit" class="btn">
                        <i class="tt-newsletter__text-wait"></i>
                        <span class="tt-newsletter__text-default">Subscribe!</span>
                        <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                        <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                    </button>
                </form>
            </div>
            <div class="tt-newsletter-popup__social">
                <div class="tt-social-icons tt-social-icons--style-03">
                    <a href="#" class="tt-btn">
                        <i class="icon-facebook"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-twitter"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-gplus"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-instagram-1"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-youtube-play"></i>
                    </a>
                </div>
            </div>
            <label class="tt-newsletter-popup__show_popup tt-checkbox">
                <input type="checkbox" name="show-nawslatter">
                <span></span>
                Don't show this popup again
            </label>
        </div>
    </main>



@endsection