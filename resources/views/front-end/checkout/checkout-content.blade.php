@extends('front-end.master')

@section('title')
    Checkout
@endsection

@section('body')
    <main>

        <div class="tt-layout tt-sticky-block__parent ">
            <div class="tt-layout__content">
                <div class="container">
                    <div class="tt-page__breadcrumbs">
                        <ul class="tt-breadcrumbs">
                            <li><a href="index.html"><i class="icon-home"></i></a></li>
                            <li><a href="index.html?page=listing-collections.html">Collections</a></li>
                            <li><span>Audio</span></li>
                        </ul>
                    </div>

                    <div class="tt-page__name text-center">
                        <h1>Checkout</h1>
                    </div>
                    @if(Session::get('customerId') )
                        <div class="row">
                             <div class="tt-summary text-center text-success" style="margin: 0px -5px; padding: 10px 10px">
                                 <span>Dear {{Session::get('name')}}. You have to give us product shipping info to complet your valuable order. if your billing info & shipping info are same then select payment and press on <b>PlaceOrder</b> button.</span>
                             </div>
                        </div>
                     @else

                     @endif
                    <div class="tt-checkout">
                        <div class="row">
                            <div class="col-lg-7">
                                <form action="{{ route('new-shipping') }}" method="post">
                                    {{ csrf_field() }}
                                    <h4 class="ttg-mt--50 ttg-mb--20">Shipping Address</h4>
                                    <div class="tt-checkout__form">

                                        @if(Session::get('customerId') )
                                        <div class="row">
                                            <div class="col-md-3"><p>Full Name:</p></div>
                                            <div class="col-md-9">
                                                <div class="tt-input">
                                                    <input name="name" value="{{ $customer->first_name.' '. $customer->last_name  }}" type="text" class="form-control colorize-theme6-bg" placeholder="Enter please your full name">
                                                </div>
                                            </div>

                                            <div class="col-md-3"><p>Email Address:</p></div>
                                            <div class="col-md-9">
                                                <div class="tt-input">
                                                    <input name="email_address" value="{{ $customer->email_address }}" type="text" class="form-control colorize-theme6-bg" placeholder="exmple@email.com">
                                                </div>
                                            </div>
                                            <div class="col-md-3"><p>Phone Number:</p></div>
                                            <div class="col-md-9">
                                                <div class="tt-input">
                                                    <input name="phone_number" value="{{ $customer->phone_number }}" type="text" class="form-control colorize-theme6-bg" placeholder="Enter please your phone number">
                                                </div>
                                            </div>

                                            <div class="col-md-3"><p>Address:</p></div>
                                            <div class="col-md-9">
                                                <div class="tt-input">
                                                    <textarea name="address" type="text" class="form-control colorize-theme6-bg" placeholder="Enter please your address">{{ $customer->address }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                            <div class="row">
                                                <div class="col-md-3"><p>Full Name:</p></div>
                                                <div class="col-md-9">
                                                    <div class="tt-input">
                                                        <input name="name" type="text" class="form-control colorize-theme6-bg" placeholder="Enter please your full name">
                                                    </div>
                                                </div>

                                                <div class="col-md-3"><p>Email Address:</p></div>
                                                <div class="col-md-9">
                                                    <div class="tt-input">
                                                        <input name="email_address" type="text" class="form-control colorize-theme6-bg" placeholder="exmple@email.com">
                                                    </div>
                                                </div>
                                                <div class="col-md-3"><p>Phone Number:</p></div>
                                                <div class="col-md-9">
                                                    <div class="tt-input">
                                                        <input name="phone_number" type="text" class="form-control colorize-theme6-bg" placeholder="Enter please your phone number">
                                                    </div>
                                                </div>

                                                <div class="col-md-3"><p>Address:</p></div>
                                                <div class="col-md-9">
                                                    <div class="tt-input">
                                                        <input name="address" type="text" class="form-control colorize-theme6-bg" placeholder="Enter please your address">
                                                    </div>
                                                </div>
                                            </div>
                                       @endif
                                    </div>

                                    <h4 class="ttg-mt--30 ttg-mb--20">Payment</h4>
                                    <div class="tt-checkout__payment">
                                        <ul>
                                            <li>Check / Money order</li>
                                            <li>
                                                <label class="tt-checkbox-circle">
                                                    <input name="payment_type" type="radio" value="Cash on Delivery">
                                                    <span></span>
                                                </label>
                                               Cash on Delivery
                                            </li>
                                            <li>
                                                <label class="tt-checkbox-circle">
                                                    <input name="payment_type" type="radio" value="Bkash">
                                                    <span></span>
                                                </label>
                                                Bkash
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="tt-checkout--border">
                                        <button type="submit" name="btn" class="tt-checkout__btn-order btn btn-type--icon colorize-btn6"><i class="icon-check"></i><span>PlaceOrder</span></button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-5">
                                <div class="tt-summary">
                                    <div class="tt-summary--border">
                                        <h4>Order Summary</h4>
                                    </div>
                                    <div class="tt-summary__products tt-summary--border">
                                        <ul>
                                            @foreach($cartProducts as $cartProduct)
                                            <li>
                                                <div>
                                                    <a href="#"><img src="{{ asset($cartProduct->options->image) }}"
                                                                     alt="Image name"></a>
                                                </div>
                                                <div>
                                                    <p><a href="#">{{ $cartProduct->name }}</a></p>
                                                        <span class="tt-summary__products_price">
                                                            <span class="tt-summary__products_price-count">{{ $cartProduct->qty }}</span>
                                                            <span>x</span>
                                                            <span class="tt-summary__products_price-val">
                                                                <span class="tt-price">
                                                                    <span>BDT. {{ $cartProduct->price }}</span>
                                                                </span>
                                                            </span>
                                                        </span>
                                                    <div class="tt-summary__products_param-control tt-summary__products_param-control--open">
                                                        <span>Details</span>
                                                        <i class="icon-down-open"></i>
                                                    </div>
                                                    <div class="tt-summary__products_param">
                                                        <span class="tt-summary__products_color">Color: <span>Orange</span></span>
                                                        <span class="tt-summary__products_size">Size: <span>XL</span></span>
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <script>
                                        require(['app'], function () {
                                            require(['modules/toggleProductParam']);
                                        });
                                    </script>
                                    <div class="tt-summary--border">
                                        <div class="tt-summary__total">
                                            <p>Subtotal: <span>BDT. {{ Session::get('orderTotal') }} </span></p>
                                        </div>

                                        <div class="tt-summary__total tt-summary__total--m-price-50">
                                            <p>Total: <span>BDT. {{ Session::get('orderTotal') }}</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tt-add-to-cart" data-active="true">
            <i class="icon-check"></i>
            <p>Added to Cart Successfully!</p>
            <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
        </div>

        <div class="tt-newsletter-popup" data-active="true">
            <div class="tt-newsletter-popup__text-01">
                <span>15</span>
                <span>
            <span>%</span>
            <span>off</span>
        </span>
            </div>
            <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
            <p>By signing up, you accept the terms & Privacy Policy.</p>
            <div class="ttg-mb--30">
                <form action="#" class="tt-newsletter tt-newsletter--style-02">
                    <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                    <button type="submit" class="btn">
                        <i class="tt-newsletter__text-wait"></i>
                        <span class="tt-newsletter__text-default">Subscribe!</span>
                        <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                        <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                    </button>
                </form>
            </div>
            <div class="tt-newsletter-popup__social">
                <div class="tt-social-icons tt-social-icons--style-03">
                    <a href="#" class="tt-btn">
                        <i class="icon-facebook"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-twitter"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-gplus"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-instagram-1"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-youtube-play"></i>
                    </a>
                </div>
            </div>
            <label class="tt-newsletter-popup__show_popup tt-checkbox">
                <input type="checkbox" name="show-nawslatter">
                <span></span>
                Don't show this popup again
            </label>
        </div>
    </main>

@endsection