<!DOCTYPE html>
<html lang="en-US">
<head>
    <base href ="/">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Mogo - Responsive HTML5 Template">
    <meta name="author" content="etheme.com">
    <link rel="shortcut icon" href="favicon.png">

    <title>@yield('title')</title>

    <!-- STYLESHEET -->
    <!-- FONTS -->
    <!-- Muli -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800%7CMontserrat:300,400,500,600,700%7COpen+Sans">

    <!-- icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="fonts/icons/fontawesome/font-awesome.css">
    <!-- MyFont -->
    <link rel="stylesheet" href="fonts/icons/myfont/css/myfont.css">
    <link rel="stylesheet" href="fonts/icons/myfont/css/myfont-embedded.css">
    <link rel="stylesheet" href="fonts/icons/myfont/css/animation.css">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="fonts/icons/myfont/css/myfont-ie7.css">
    <![endif]-->

    <!-- Vendor -->
    <link rel="stylesheet" href="vendor/bootstrap/v3/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/bootstrap/v4/css/bootstrap-grid.css">
    <link rel="stylesheet" href="vendor/perfectScrollbar/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" href="vendor/tonymenu/css/tonymenu.css">
    <link rel="stylesheet" href="vendor/revolution/css/settings.css">
    <link rel="stylesheet" href="vendor/revolution/css/layers.css">
    <link rel="stylesheet" href="vendor/revolution/css/navigation.css">
    <link rel="stylesheet" href="vendor/slick/slick.min.css">
    <link rel="stylesheet" href="vendor/magnificPopup/dist/magnific-popup.css">
    <link rel="stylesheet" href="vendor/rangeSlider/css/ion.rangeSlider.css">
    <link rel="stylesheet" href="vendor/rangeSlider/css/ion.rangeSlider.skinFlat.css">
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">
    <link rel="stylesheet" href="vendor/fotorama/fotorama.css">


    <!-- Custom -->
    <link rel="stylesheet" href="css/style.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!--- Admin content--->
    {{--<link href="{{asset('/')}}/admin-front-end/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">--}}

    <!-- MetisMenu CSS -->
    {{--<link href="{{asset('/')}}/admin-front-end/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">--}}

    {{--<!-- Custom CSS -->--}}
    {{--<link href="{{asset('/')}}/admin-front-end/dist/css/sb-admin-2.css" rel="stylesheet">--}}

    {{--<!-- Morris Charts CSS -->--}}
    {{--<link href="{{asset('/')}}/admin-front-end/vendor/morrisjs/morris.css" rel="stylesheet">--}}

    {{--<!-- Custom Fonts -->--}}
    {{--<link href="{{ asset('/')}}/admin-front-end/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">--}}


</head>
<body id="theme">

<!-- HEADER -->
@include('front-end.includes.header')

<!-- MAIN -->

@yield('body')

<!-- FOOTER -->
@include('front-end.includes.footer')

<!-- JAVA SCRIPT -->
<!--plugins-->
<script src="vendor/scrollSmooth/SmoothScroll.js"></script>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/cookie/jquery.cookie.js"></script>
<script src="vendor/jquery/ui/jquery-ui.min.js"></script>
<script src="vendor/velocity/velocity.min.js"></script>
<script src="vendor/modernizr/modernizr.js"></script>
<script src="vendor/lazyLoad/jquery.lazy.min.js"></script>
<script src="vendor/lazyLoad/jquery.lazy.plugins.min.js"></script>
<script src="vendor/tonymenu/js/tonymenu.js"></script>
<script src="vendor/perfectScrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="vendor/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="vendor/countdown/jquery.countdown.min.js"></script>
<script src="vendor/moment/moment.js"></script>
<script src="vendor/moment/moment-timezone.js"></script>
<script src="vendor/slick/slick.min.js"></script>
<script src="vendor/magnificPopup/dist/jquery.magnific-popup.min.js"></script>
<script src="vendor/elevateZoom/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="vendor/stickyBlock/sticky-sidebar.min.js"></script>
<script src="vendor/rangeSlider/js/ion.rangeSlider.min.js"></script>
<script src="vendor/instafeed/instafeed.min.js"></script>
<script src="vendor/jquery/jquery-bridget.js"></script>
<script src="vendor/imagesLoaded/imagesloaded.pkgd.min.js"></script>
<script src="vendor/masonry/masonry.pkgd.min.js"></script>
<script src="vendor/swiper/js/swiper.min.js"></script>
<script src="vendor/fotorama/fotorama.js"></script>


<!--modules-->
<script src="js/app.js"></script>

</body>
</html>
