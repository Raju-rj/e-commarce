@extends ('front-end.master')

@section('body')

    <main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                <div class="tt-page__breadcrumbs">
                    <ul class="tt-breadcrumbs">
                        <li><a href="index.html"><i class="icon-home"></i></a></li>
                        <li><a href="index.html?page=listing-collections.html">Collections</a></li>
                        <li><span>Audio</span></li>
                    </ul>
                </div>

                <div class="tt-page__name text-center">
                    <h1>About Store</h1>
                    <p>Dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
                </div>

                <div class="tt-about">
                    <div class="row ttg-grid-padding--none">
                        <div class="col-md-6">
                            <a href="$" class="ttg-image-translate--left ttg-animation-disable--sm"><img
                                    data-srcset="images/about/about-01.jpg" alt="Image name"></a>
                        </div>
                        <div class="col-md-6">
                            <div class="tt-about__info">
                                <div>Interesting Facts</div>
                                <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                    irure dolor in reprehenderit in voluptate velit esse cillum dolore.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tt-about__info">
                                <div>Best Brands</div>
                                <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                    irure dolor in reprehenderit in voluptate velit esse cillum dolore.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a href="$" class="ttg-image-scale ttg-animation-disable--sm"><img
                                    data-srcset="images/about/about-02.jpg" alt="Image name"></a>
                        </div>
                        <div class="col-md-6">
                            <a href="$" class="ttg-image-translate--bottom ttg-animation-disable--sm"><img
                                    data-srcset="images/about/about-03.jpg" alt="Image name"></a>
                        </div>
                        <div class="col-md-6">
                            <a href="$" class="ttg-image-translate--right ttg-animation-disable--sm"><img
                                    data-srcset="images/about/about-04.jpg" alt="Image name"></a>
                        </div>
                    </div>
                </div>

                <div class="tt-page__name-sm text-center">
                    <h2>Our Team</h2>
                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore.</p>
                </div>



                <div class="tt-page__name-sm text-center">
                    <h2>Interested in Working Together?</h2>
                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore.</p>
                    <a href="#" class="btn">Get in touch with us</a>
                </div>

                <div class="tt-post ttg-mt--100">
                    <div class="tt-post__slider tt-post__slider--bg">
                        <div class="tt-post__content-quote">
                            <i class="icon-quote-1"></i>
                            <div class="tt-post__content-quote_title">Lorem ipsum dolor sit amet conse ctetur
                                adipisicing elit!
                            </div>
                            <p class="colorize-text-c">Dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                                et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip.</p>
                            <div class="tt-post__content-quote_quote">– Anis Anderson</div>
                        </div>
                        <div class="tt-post__content-quote">
                            <i class="icon-quote-1"></i>
                            <div class="tt-post__content-quote_title">Dolor sit amet conse ctetur adipisicing elit, sed
                                do eiusmod
                            </div>
                            <p class="colorize-text-c">Dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                                et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip.</p>
                            <div class="tt-post__content-quote_quote">– Anis Anderson</div>
                        </div>
                        <div class="tt-post__content-quote">
                            <i class="icon-quote-1"></i>
                            <div class="tt-post__content-quote_title">Ipsum dolor sit amet conse ctetur adipisicing
                                elit!
                            </div>
                            <p class="colorize-text-c">Dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                                et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip.</p>
                            <div class="tt-post__content-quote_quote">– Anis Anderson</div>
                        </div>
                    </div>
                    <div class="tt-post__slider-nav tt-post__slider-nav--fixed-c tt-post__slider-nav--dark tt-post__slider-nav--arrows-none"></div>
                </div>
                <script>
                    require(['app'], function () {
                        require(['modules/sliderBlog']);
                    });
                </script>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
        <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection
