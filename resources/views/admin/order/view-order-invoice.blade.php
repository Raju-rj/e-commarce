<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

    <title>Editable Invoice</title>

    {{--<link rel='stylesheet' type='text/css' href='{{ asset('/') }}invoice/css/style.css' />--}}
    {{--<link rel='stylesheet' type='text/css' href='{{ asset('/') }}invoice/css/print.css' media="print" />--}}
    {{--<script type='text/javascript' src='{{ asset('/') }}invoice/js/jquery-1.3.2.min.js'></script>--}}
    {{--<script type='text/javascript' src='{{ asset('/') }}invoice/js/example.js'></script>--}}

    <style type="text/css">
        h3{
            background: orangered;
            padding: 10px 10px;
            text-align: center;
            color: white;
        }

        .bill{

        }



        table{
            width: 100%;
        }

        table, th, td{
            border: 1px solid black;
            border-collapse: collapse;
            opacity: 0.95;
        }

        th, td{

            padding: 6px;
        }

        th{
            background: lightgrey;
            color: black;
        }
        .shipping{
            float: left;
        }

        .orderid{
            text-align: right;
            float: right;
        }

        img{
            float: right;
        }

    </style>

</head>

<body style="margin: auto; width: 800px">

<div class="container">
    <h3>I  N  V  O  I  C  E</h3>


    <img src="{{ asset('/') }}invoice/images/logo.png" width="200px" alt="logo" />
    <div>
        <div class="bill">
        <h4>BILL FROM</h4>
            @if(Session::get('customerId') )
            <span>{{ $customer->first_name. ' ' . $customer->last_name }}</span><br/>
                  <span>{{ $customer->address }}</span><br/>
            <span>Email:{{ $customer->email_address }}</span><br/>
                 <span>Phone:{{ $customer->phone_number }}</span>
            @else
                <span>{{ $shipping->name }}</span><br/>
                <span>{{ $shipping->address }}</span><br/>
                <span>Email:{{ $shipping->email_address }}</span><br/>
                <span>Phone:{{ $shipping->phone_number }}</span>
            @endif

        </div>

    </div>



    <br/>
    <hr style="color: lightgrey">


    <div>
        <div class="shipping">
            <h4>SHIPPING TO</h4>
             <span>{{ $shipping->name }}</span><br/>
             <span>{{ $shipping->address }}</span><br/>
             <span>email:{{ $shipping->email_address }}</span><br/>
             <span>Phone:{{ $shipping->phone_number }}</span>
        </div>

        <table style="margin-top: 20px; text-align: left; background: white; width: 270px" align="right">

            <tr><th>Invoice #</th><td>00000{{ $order->id }}</td></tr>

            <tr><th>Date :</th> <td>{{ $order->created_at }}</td></tr>

            <tr><th>Amount Due</th><td>Tk. {{ $order->order_total }}</td></tr>

        </table>

        {{--<div class="orderid">--}}
        {{--<strong>Invoice #</strong>--}}
        {{--<span>000123</span><br/>--}}
        {{--<strong>Date: </strong>--}}
        {{--<span>July 04, 2018</span><br/>--}}
        {{--<strong>Amount Due</strong>--}}
        {{--<span>000123</span>--}}
        {{--</div>--}}


    </div>

    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>

    <table align="center">

        <tr>
            <th>Item</th>
            <th>Description</th>
            <th>Unit Cost</th>
            <th>Quantity</th>
            <th>Price</th>
        </tr>
        @php($sum = 0)
        @foreach($orderDetails as $orderDetail)
            <tr align="right">
                <td class="item-name">{{ $orderDetail->product_name }}</td>
                <td class="description">demo</td>
                <td>{{ $orderDetail->product_price }}</td>
                <td>{{ $orderDetail->product_quantity }}</td>
                <td align="right">Tk. {{ $total = $orderDetail->product_price * $orderDetail->product_quantity }}</td>
            </tr>
            <?php $sum = $sum + $total ?>
         @endforeach

    </table>
   <br>

    <table align="right" style="width: 266px; text-align: left">
        <tr>
            {{--<td colspan="2" class="blank"> </td>--}}
            <th colspan="4" class="total-line">Subtotal</th>
            <td align="right"><div id="subtotal">Tk. {{ $sum }}</div></td>
        </tr>
        <tr>

            {{--<td colspan="2" class="blank"> </td>--}}
            <th colspan="4" class="total-line">Total</th>
            <td align="right"><div id="total">Tk. {{ $sum }}</div></td>
        </tr>
    </table>

    <div style="margin-top: 100px; margin-bottom: 20px">
        <h4 style="margin: 0px">Notice</h4>
        <span>NET 30 Days. Finance Charge of 1.5% will be made on unpaid balances after 30 days.</span>
    </div>

</div>

</body>

</html>