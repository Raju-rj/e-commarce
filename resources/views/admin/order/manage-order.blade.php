
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Order List</h4>
                </div>
                <div class="panel-body">
                    <h4 class="text-center text-danger">{{session('unpublished')}}</h4>
                    <h4 class="text-center text-success">{{session('published')}}</h4>
                    <h4 class="text-center text-success">{{session('update')}}</h4>
                    <h4 class="text-center text-danger">{{session('delete')}}</h4>

                    <table width="100%" class="table table-bordered" id="dataTables-example">

                        <thead>
                        <tr class="bg-primary">
                            <th>S.l No</th>
                            <th>Customer Name</th>
                            <th>Order Total (Tk.)</th>
                            <th>Order Status</th>
                            <th>Pyament Type</th>
                            <th>Pyament Status</th>
                            <th>Order Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        @php($i = 1)
                        @foreach($orders as $order)
                            <tbody>
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{ $order->name }}</td>
                                <td class="text-right">{{ $order->order_total }}</td>
                                <td class="text-center">{{ $order->order_status }}</td>
                                <td>{{ $order->payment_type }}</td>
                                <td>{{ $order->payment_status }}</td>
                                <td>{{ $order->created_at }}</td>

                                <td>

                                    <a href="{{ route('view-order-detail', ['id'=>$order->id]) }}" class="btn btn-success btn-xs" title="View Order Details">
                                        <span class="glyphicon glyphicon-zoom-in"></span>
                                    </a>

                                    <a href="{{ route('view-order-invoice', ['id'=>$order->id]) }}" class="btn btn-default btn-xs" title="View Order Invoice">
                                        <span class="glyphicon glyphicon-zoom-out"></span>
                                    </a>

                                    <a href="{{ route('download-order-invoice', ['id'=>$order->id]) }}" class="btn btn-primary btn-xs" title="Download Order Invoice">
                                        <span class="glyphicon glyphicon-download"></span>
                                    </a>

                                    <a href="{{ route('delete-category', ['id'=>$order->id]) }}" class="btn btn-success btn-xs" title="Eidt Order">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>

                                    <a href="{{ route('delete-category', ['id'=>$order->id]) }}" class="btn btn-danger btn-xs" title="Delete Order">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>

                                </td>
                            </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

