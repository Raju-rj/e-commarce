
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

    <title>Editable Invoice</title>

    {{--<link rel='stylesheet' type='text/css' href='{{ asset('/') }}invoice/css/style.css' />--}}
    {{--<link rel='stylesheet' type='text/css' href='{{ asset('/') }}invoice/css/print.css' media="print" />--}}
    {{--<script type='text/javascript' src='{{ asset('/') }}invoice/js/jquery-1.3.2.min.js'></script>--}}
    {{--<script type='text/javascript' src='{{ asset('/') }}invoice/js/example.js'></script>--}}

    <style type="text/css">
        h3{
            margin-top: 0px;
            background: orangered;
            padding: 10px 10px;
            text-align: center;
            color: white;
        }

        table{
            width: 100%;
        }

        table, th, td{
            border: 1px solid black;
            border-collapse: collapse;
            opacity: 0.95;
        }

        th, td{

            padding: 6px;
        }

        th{
            background: lightgrey;
            color: black;
        }
        .shipping{
           float: left;
        }

        .orderid{
            text-align: right;
            float: right;
        }

        img{
            float: right;
        }
    </style>

</head>

<body>

<div class="container">
    <h3>I  N  V  O  I  C  E</h3>
    <div>
        <img src="{{ asset('/') }}invoice/images/logo.png" width="200px" alt="logo" />
        <h4>BILL FROM</h4>
        @if(Session::get('customerId') )
            <span>{{ $customer->first_name. ' ' . $customer->last_name }}</span><br/>
            <span>{{ $customer->address }}</span><br/>
            <span>Email:{{ $customer->email_address }}</span><br/>
            <span>Phone:{{ $customer->phone_number }}</span>
        @else
            <span>{{ $shipping->name }}</span><br/>
            <span>{{ $shipping->address }}</span><br/>
            <span>Email:{{ $shipping->email_address }}</span><br/>
            <span>Phone:{{ $shipping->phone_number }}</span>
        @endif

    </div>
<br/>
    <hr style="color: lightgrey">


    <div>
        <div class="shipping">
        <h4>SHIPPING TO</h4>
            <span>{{ $shipping->name }}</span><br/>
            <span>{{ $shipping->address }}</span><br/>
            <span>email:{{ $shipping->email_address }}</span><br/>
            <span>Phone:{{ $shipping->phone_number }}</span>
        </div>

        <table style="margin-top: 20px; background: white; width: 300px" align="right">

            <tr><th>Invoice #</th><td>00000{{ $order->id }}</td></tr>

            <tr><th>Date :</th> <td>{{ $order->created_at }}</td></tr>

            <tr><th>Amount Due</th><td>Tk. {{ $order->order_total }}</td></tr>

        </table>
    </div>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>


    <table align="center">

        <tr align="center">
            <th>S.L</th>
            <th>Product Item</th>
            <th>Unit Price</th>
            <th>Quantity</th>
            <th>Price</th>
        </tr>
        @php( $i = 1 )
        @php($sum = 0)
        @foreach($orderDetails as $orderDetail)
            <tr align="right">
                <td align="center">{{ $i++ }}</td>
                <td align="left">{{ $orderDetail->product_name }}</td>
                <td>Tk. {{ $orderDetail->product_price }}</td>
                <td>{{ $orderDetail->product_quantity }}</td>
                <td align="right">Tk. {{ $total = $orderDetail->product_price * $orderDetail->product_quantity }}</td>
            </tr>
            <?php $sum = $sum + $total ?>
        @endforeach

        {{--<tr class="item-row">--}}
            {{--<td class="item-name">SSL Renewals</td>--}}

            {{--<td class="description">Yearly renewals of SSL certificates on main domain and several subdomains</td>--}}
            {{--<td>$75.00</td>--}}
            {{--<td>3</td>--}}
            {{--<td><span class="price">$225.00</span></td>--}}
        {{--</tr>--}}


    </table>
<br/>
    <table align="right" style="width: 267px;">
        <tr>
            <th colspan="4" class="total-line">Subtotal</th>
            <td align="right"><div id="subtotal">Tk. {{ $sum }}</div></td>
        </tr>
        <tr>
            <th colspan="4" class="total-line">Shipping Charge</th>
            <td align="right"><div id="total">Tk. 0</div></td>
        </tr>
        <tr>
            <th colspan="4" class="total-line">Total</th>
            <td align="right"><div id="total">Tk. {{ $sum }}</div></td>
        </tr>
        {{--<tr>--}}
            {{--<td colspan="2" class="blank"> </td>--}}
            {{--<th colspan="4" class="total-line">Amount Paid</th>--}}
            {{--<td class="total-value">$0.00</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--<td style="mso-hide: all" colspan="2" class="blank"> </td>--}}
            {{--<th colspan="4" class="total-line balance">Balance Due</th>--}}
            {{--<td class="total-value balance"><div class="due">$875.00</div></td>--}}
        {{--</tr>--}}
    </table>

    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>


    <div id="terms">
        <h4>Notice</h4>
        <span>NET 30 Days. Finance Charge of 1.5% will be made on unpaid balances after 30 days.</span>
    </div>

</div>

</body>

</html>