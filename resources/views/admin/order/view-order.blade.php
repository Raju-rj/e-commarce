
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Shipping info For This Order</h4>
                </div>
                <div class="panel-body">
                    <h4 class="text-center text-danger">{{session('unpublished')}}</h4>
                    <h4 class="text-center text-success">{{session('published')}}</h4>
                    <h4 class="text-center text-success">{{session('update')}}</h4>
                    <h4 class="text-center text-danger">{{session('delete')}}</h4>

                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Customer Name</th>
                            <td>{{ $shipping->name }}</td>
                        </tr>
                        <tr>
                            <th>Email Address</th>
                            <td>{{ $shipping->email_address }}</td>
                        </tr>
                        <tr>
                            <th>Phone Number</th>
                            <td>{{ $shipping->phone_number }}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>{{ $shipping->address }}</td>
                        </tr>
                        <tr>
                            <th>Payment Type</th>
                            <td>{{ $shipping->payment_type }}</td>
                        </tr>
                        <tr>
                            <th>Payment Status</th>
                            <td>{{ $shipping->payment_status }}</td>
                        </tr>
                        <tr>
                            <th>Order Date</th>
                            <td>{{ $shipping->created_at }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Product info For This Order</h4>
                </div>
                <div class="panel-body">

                    <table width="100%" class="table table-bordered" id="dataTables-example">

                        <thead>
                        <tr class="bg-primary">
                            <th>S.l No</th>
                            <th>Product Id</th>
                            <th>Product Name</th>
                            <th>Product Price</th>
                            <th>Product Quantity</th>
                            <th>Total Price</th>
                        </tr>
                        </thead>
                            <tbody>
                            @php( $i = 1)
                                @foreach($orderDetails as $orderDetail)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $orderDetail->product_id }}</td>
                                        <td>{{ $orderDetail->product_name }}</td>
                                        <td>{{ $orderDetail->product_price }}</td>
                                        <td>{{ $orderDetail->product_quantity }}</td>
                                        <td>{{ $orderDetail->product_price * $orderDetail->product_quantity }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

