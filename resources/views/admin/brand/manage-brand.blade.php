
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Brand List</h4>
                </div>
                <div class="panel-body">
                    <h4 class="text-center text-danger">{{session('unpublished')}}</h4>
                    <h4 class="text-center text-success">{{session('published')}}</h4>
                    <h4 class="text-center text-success">{{session('update')}}</h4>
                    <h4 class="text-center text-danger">{{session('delete')}}</h4>

                    <table width="100%" class="table table-bordered" id="dataTables-example">

                        <thead>
                            <tr>
                                <th>S.l No</th>
                                <th>Brand Name</th>
                                <th>Brand Description</th>
                                <th>Publoication Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        @php($i = 1)
                        @foreach($brands as $brand)
                        <tbody>
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$brand->brand_name}}</td>
                                <td>{{$brand->brand_description}}</td>
                                <td>{{$brand->publication_status == 1 ? 'Published' : 'Unpublished'}}</td>
                                <td>
                                    @if($brand->publication_status == 1 )
                                        <a href="{{ route('unpublished', ['id'=>$brand->id]) }}" class="btn btn-info btn-xs">
                                            <samp class="glyphicon glyphicon-arrow-up"></samp>
                                        </a>
                                    @else
                                        <a href="{{ route('published', ['id'=>$brand->id]) }}" class="btn btn-warning btn-xs">
                                            <samp class="glyphicon glyphicon-arrow-down"></samp>
                                        </a>
                                    @endif
                                    <a href="{{ route('edit-brand', ['id'=>$brand->id]) }}" class="btn btn-success btn-xs">
                                        <samp class="glyphicon glyphicon-edit"></samp>
                                    </a>
                                    <a href="{{ route('delete-brand', ['id'=>$brand->id]) }}" class="btn btn-danger btn-xs">
                                        <samp class="glyphicon glyphicon-trash"></samp>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

