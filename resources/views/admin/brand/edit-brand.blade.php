
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row m-lg-2">
        <div class="col-md-9 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Edit Brand Form</h4>
                </div>
                <div class="panel-body">
                    <h4 class="text-center text-success">{{session('message')}}</h4>
                    <form action="{{ route('update-brand') }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-4">Brand Name</label>
                            <div class="col-md-8">
                                <input type="text" name="brand_name" value="{{$brand->brand_name}}" class="form-control"/>
                                <input type="hidden" name="brand_id" value="{{$brand->id}}" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Brand Description</label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="brand_description" >{{$brand->brand_description}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Publication status</label>
                            <div class="col-md-8 radio" >
                                <label><input type="radio"  name="publication_status" {{$brand->publication_status == 1 ? 'checked' : ''}} value="1"/> Published</label>
                                <label><input type="radio"  name="publication_status" {{$brand->publication_status == 0 ? 'checked' : ''}} value="0"/> Unpublished</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <input type="submit" name="btn" class="btn btn-success btn-block" value="Update Brand Info"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

