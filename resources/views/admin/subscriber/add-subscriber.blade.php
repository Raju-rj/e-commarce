
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row m-lg-2">
        <div class="col-md-9 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Add Subscriber Form</h4>
                </div>
                <div class="panel-body">
                    <h4 class="text-center text-success">{{session('message')}}</h4>
                    <form action="{{ route('new-email') }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="control-label col-md-4">Email</label>
                            <div class="col-md-8">
                                <input type="text" name="email" class="form-control"/>
                                <span class="text-danger">{{$errors->has('email') ? $errors->first('email') : ' ' }}</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <input type="submit" name="btn" class="btn btn-success btn-block" value="Save Email Info"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

