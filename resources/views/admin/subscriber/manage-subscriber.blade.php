
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row m-lg-2">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Email List</h4>
                </div>
                <div class="panel-body">

                    <h4 class="text-center text-danger">{{session('delete')}}</h4>

                    <table width="100%" class="table table-bordered" id="dataTables-example">

                        <thead>
                        <tr class="bg-primary">
                            <th>S.l No</th>
                            <th>Email Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        @php($i = 1)
                        @foreach($emails as $email)
                            <tbody>
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$email->email}}</td>
                                <td>
                                    <a href="{{ route('delete-subscriber', ['id'=>$email->id]) }}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>

                                </td>
                            </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

