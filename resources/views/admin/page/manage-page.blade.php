
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row m-lg-2">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Page List</h4>
                </div>
                <div class="panel-body">
                    <h4 class="text-center text-danger">{{session('unpublished')}}</h4>
                    <h4 class="text-center text-success">{{session('published')}}</h4>
                    <h4 class="text-center text-success">{{session('update')}}</h4>
                    <h4 class="text-center text-danger">{{session('delete')}}</h4>

                    <table width="100%" class="table table-bordered" id="dataTables-example">

                        {{--class="table table-striped table-bordered table-hover" id="dataTables-example"--}}
                        <thead>
                        <tr class="bg-primary">
                            <th>S.l No</th>
                            <th>Page Title</th>
                            <th>Page Content</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        @php($i = 1)
                        @foreach($pages as $page)
                            <tbody>
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$page->page_title}}</td>
                                <td>{{$page->page_content}}</td>
                                <td>{{$page->publication_status == 1 ? 'Published' : 'Unpublished'}}</td>
                                <td>
                                    @if($page->publication_status == 1)
                                        <a href="{{ route('page-unpublished', ['id'=>$page->id]) }}" class="btn btn-info btn-xs">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{ route('page-published', ['id'=>$page->id]) }}" class="btn btn-warning btn-xs">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                    <a href="{{ route('edit-page', ['id'=>$page->id]) }}" class="btn btn-success btn-xs">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="{{ route('delete-page', ['id'=>$page->id]) }}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>

                                        <a href="{{ route('show-page', ['id'=>$page->id]) }}" class="btn btn-primary btn-xs">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </a>

                                </td>
                            </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

