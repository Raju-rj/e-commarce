
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row m-lg-2">
        <div class="col-md-9 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Add Page Form</h4>
                </div>
                <div class="panel-body">
                    <h4 class="text-center text-success">{{session('message')}}</h4>
                    <form action="{{ route('new-page') }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-4">Page Title</label>
                            <div class="col-md-8">
                                <input type="text" name="page_title" class="form-control"/>
                                <span class="text-danger">{{$errors->has('page_title') ? $errors->first('page_title') : ' ' }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Page Content</label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="page_content" ></textarea>
                                <span class="text-danger">{{$errors->has('page_content') ? $errors->first('page_content') : ' ' }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Publication status</label>
                            <div class="col-md-8 radio" >
                                <label><input type="radio" name="publication_status" value="1"/> Published</label>
                                <label><input type="radio"  name="publication_status" value="0"/> Unpublished</label> <br/>
                                <span class="text-danger">{{$errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <input type="submit" name="btn" class="btn btn-success btn-block" value="Save Page Info"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

