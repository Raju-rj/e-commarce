
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row m-lg-2">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Page List</h4>
                </div>
                <div class="panel-body">

                    <dl>
                        <dt>{{ $page->page_title }}</dt>
                        <dd>{{ $page->page_content }}</dd>
                    </dl>

                </div>
            </div>
        </div>
    </div>


@endsection

