
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Category List</h4>
                </div>
                <div class="panel-body">
                    <h4 class="text-center text-danger">{{session('unpublished')}}</h4>
                    <h4 class="text-center text-success">{{session('published')}}</h4>
                    <h4 class="text-center text-success">{{session('update')}}</h4>
                    <h4 class="text-center text-danger">{{session('delete')}}</h4>

                    <table width="100%" class="table table-bordered" id="dataTables-example">

                        {{--class="table table-striped table-bordered table-hover" id="dataTables-example"--}}
                    <thead>
                        <tr class="bg-primary">
                            <th>S.l No</th>
                            <th>Category Name</th>
                            <th>Category Description</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                        <tr>
                            <td>S.l</td>
                            <td>category_name</td>
                            <td>category_description</td>
                            <td>publication_status</td>
                            <td><a href="" class="btn btn-success btn-xs">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                            </td>
                        </tr>
                        @php($i = 1)
                        @foreach($categories as $category)
                    <tbody>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$category->category_name}}</td>
                            <td>{{$category->category_description}}</td>
                            <td>{{$category->publication_status == 1 ? 'Published' : 'Unpublished'}}</td>
                            <td>
                                @if($category->publication_status == 1)
                                    <a href="{{ route('category-unpublished', ['id'=>$category->id]) }}" class="btn btn-info btn-xs">
                                        <span class="glyphicon glyphicon-arrow-up"></span>
                                    </a>
                                @else
                                    <a href="{{ route('category-published', ['id'=>$category->id]) }}" class="btn btn-warning btn-xs">
                                        <span class="glyphicon glyphicon-arrow-down"></span>
                                    </a>
                                @endif
                                <a href="{{ route('edit-category', ['id'=>$category->id]) }}" class="btn btn-success btn-xs">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                                <a href="{{ route('delete-category', ['id'=>$category->id]) }}" class="btn btn-danger btn-xs">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>

                            </td>
                        </tr>
                    </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

