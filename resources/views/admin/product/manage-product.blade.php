
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Product List</h4>
                </div>
                <div class="panel-body">
                    <h4 class="text-center text-danger">{{session('unpublished')}}</h4>
                    <h4 class="text-center text-success">{{session('published')}}</h4>
                    <h4 class="text-center text-success">{{session('update')}}</h4>
                    <h4 class="text-center text-danger">{{session('delete')}}</h4>

                    <table width="100%" class="table table-bordered" id="dataTables-example">

                        <thead>
                        <tr class="bg-primary">
                            <th>S.l No</th>
                            <th>Category Name</th>
                            <th>Brand Name</th>
                            <th>Product Name</th>
                            <th>Product Image</th>
                            <th>Product Price</th>
                            <th>Product Quantity</th>
                            <th>Publoication Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        @php($i=1)
                        @foreach($products as $product)
                           <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $product->category_name }}</td>
                                <td>{{ $product->brand_name }}</td>
                                <td>{{ $product->product_name }}</td>
                                <td>
                                    <img src="{{ asset($product->product_image) }}" alt="" height="100" width="200">
                                </td>
                                <td>{{ $product->product_price }}</td>
                                <td>{{ $product->product_quantity}}</td>
                                <td>{{ $product->publication_status }}</td>
                                <td>

                                    <a href=""
                                       class="btn btn-success btn-xs" title="View Details">
                                        <span class="glyphicon glyphicon-zoom-in"></span>
                                    </a>
                                    @if($product->publication_status == 1)
                                        <a href="{{ route('product-unpublished', ['id'=>$product->id]) }}"
                                           class="btn btn-info btn-xs" title="Published">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{ route('product-published', ['id'=>$product->id]) }}"
                                           class="btn btn-warning btn-xs" title="Unpublished">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                    <a href="{{ route('edit-product', ['id'=>$product->id]) }}"
                                       class="btn btn-success btn-xs" title="Edit product">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="{{ route('delete-product', ['id'=>$product->id]) }}"
                                       class="btn btn-danger btn-xs" title="Delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                           </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

