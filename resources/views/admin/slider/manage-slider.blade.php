
@extends('admin-layouts/master-layout')

@section('body')
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-success">Slider List</h4>
                </div>
                <div class="panel-body">
                    <h4 class="text-center text-danger">{{session('unpublished')}}</h4>
                    <h4 class="text-center text-success">{{session('published')}}</h4>
                    <h4 class="text-center text-success">{{session('update')}}</h4>
                    <h4 class="text-center text-danger">{{session('delete')}}</h4>

                    <table width="100%" class="table table-bordered" id="dataTables-example">

                        <thead>
                        <tr class="bg-primary">
                            <th>S.l No</th>
                            <th>Slider Title</th>
                            <th>Slider Image</th>
                            <th>Publoication Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        @php($i=1)
                        @foreach($sliders as $slider)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $slider->slider_title }}</td>

                                <td>
                                    <img src="{{ asset($slider->slider_image) }}" alt="" height="100" width="200">
                                </td>
                                <td>{{ $slider->publication_status }}</td>
                                <td>

                                    @if($slider->publication_status == 1)
                                        <a href="{{ route('slider-unpublished', ['id'=>$slider->id]) }}" class="btn btn-info btn-xs">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{ route('slider-published', ['id'=>$slider->id]) }}" class="btn btn-warning btn-xs">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                    <a href="{{ route('edit-slider', ['id'=>$slider->id]) }}" class="btn btn-success btn-xs">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="{{ route('delete-slider', ['id'=>$slider->id]) }}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

