<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Slider::class, function (Faker $faker) {
    return [
        'picture' =>$faker->image(public_path('uploads'), 720, 520, '',false),
        'title' =>$faker->word,
        'status'=> $faker->word,
        'created_by'       => $faker->word,
        'updated_by'       => $faker->word,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];
});
